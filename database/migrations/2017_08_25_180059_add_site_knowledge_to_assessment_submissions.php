<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSiteKnowledgeToAssessmentSubmissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assessment_submissions', function (Blueprint $table) {
            $table->integer('site_knowledge')->nullable();
        });

        DB::connection()->getPdo()->exec("update assessment_submissions set site_knowledge=1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessment_submissions', function (Blueprint $table) {
            $table->dropColumn(['site_knowledge']);
        });
    }
}
