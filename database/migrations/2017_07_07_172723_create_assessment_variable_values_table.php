<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessmentVariableValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment_variable_values', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('weighting');
            $table->string('image');

            $table->integer('assessment_variable_id')->unsigned();
            $table->foreign('assessment_variable_id')->references('id')->on('assessment_variables');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_variable_values');
    }
}
