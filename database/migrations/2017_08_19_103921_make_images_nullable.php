<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeImagesNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assessment_models', function (Blueprint $table) {
            $table->string('image')->nullable()->change();
        });

        Schema::table('assessment_categories', function (Blueprint $table) {
            $table->string('image')->nullable()->change();
        });

        Schema::table('assessment_sub_categories', function (Blueprint $table) {
            $table->string('image')->nullable()->change();
        });

        Schema::table('assessment_variables', function (Blueprint $table) {
            $table->string('image')->nullable()->change();
        });

        Schema::table('assessment_variable_values', function (Blueprint $table) {
            $table->string('image')->nullable()->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
