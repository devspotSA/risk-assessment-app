<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDisplayWeightsToHeirarchy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assessment_models', function (Blueprint $table) {
            $table->integer('display_weight')->nullable();
        });

        DB::connection()->getPdo()->exec("update assessment_models set display_weight=(id*100)");

        Schema::table('assessment_categories', function (Blueprint $table) {
            $table->integer('display_weight')->nullable();
        });

        DB::connection()->getPdo()->exec("update assessment_categories set display_weight=(id*100)");

        Schema::table('assessment_benefits', function (Blueprint $table) {
            $table->integer('display_weight')->nullable();
        });

        DB::connection()->getPdo()->exec("update assessment_benefits set display_weight=(id*100)");

        Schema::table('assessment_sub_categories', function (Blueprint $table) {
            $table->integer('display_weight')->nullable();
        });

        DB::connection()->getPdo()->exec("update assessment_sub_categories set display_weight=(id*100)");

        Schema::table('assessment_variables', function (Blueprint $table) {
            $table->integer('display_weight')->nullable();
        });

        DB::connection()->getPdo()->exec("update assessment_variables set display_weight=(id*100)");

        Schema::table('assessment_variable_values', function (Blueprint $table) {
            $table->integer('display_weight')->nullable();
        });

        DB::connection()->getPdo()->exec("update assessment_variable_values set display_weight=(id*100)");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessment_models', function (Blueprint $table) {
            $table->dropColumn(['display_weight']);
        });

        Schema::table('assessment_categories', function (Blueprint $table) {
            $table->dropColumn(['display_weight']);
        });

        Schema::table('assessment_sub_categories', function (Blueprint $table) {
            $table->dropColumn(['display_weight']);
        });

        Schema::table('assessment_variables', function (Blueprint $table) {
            $table->dropColumn(['display_weight']);
        });

        Schema::table('assessment_variable_values', function (Blueprint $table) {
            $table->dropColumn(['display_weight']);
        });
    }
}
