<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessmentSubmissionBenefitSelectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment_submission_benefit_selections', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('assessment_submission_id')->unsigned();
            $table->foreign('assessment_submission_id','asvb_as_id_foreign')->references('id')->on('assessment_submissions');

            $table->integer('assessment_benefit_id')->unsigned();
            $table->foreign('assessment_benefit_id','asvb_ben_id_foreign')->references('id')->on('assessment_benefits');

            $table->integer('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_submission_benefit_selections');
    }
}
