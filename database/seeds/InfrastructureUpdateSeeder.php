<?php

use Illuminate\Database\Seeder;
use App\AssessmentBenefit;
use App\AssessmentModel;
use App\AssessmentCategory;
use App\AssessmentSubCategory;
use App\AssessmentVariable;
use App\AssessmentVariableValue;

class InfrastructureUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->runImportModel('infrastructure_addition_v1');
    }

    public function runImportModel($model_prefix)
    {
        $files = [
            __DIR__ . "/" . $model_prefix . "_subcategory.csv",
            __DIR__ . "/" . $model_prefix . "_variable.csv",
            __DIR__ . "/" . $model_prefix . "_variable_value.csv",
        ];

        $models = [];
        foreach ($files as $file) {
            if (!file_exists($file)) {
                throw new \Exception("File missing: " . $file);
            }

            $key = 'model';
            if (preg_match('/_model.csv/', $file)) {
                $key = 'name';
            }

            $row_num = 0;
            $headers = [];
            if (($handle = fopen($file, "r")) !== FALSE) {
                while (($row = fgetcsv($handle, 4096, ",")) !== FALSE) {
                    $row_num++;

                    if ($row_num == 1) {
                        $headers = $row;
                        continue;
                    }

                    $data = [];
                    foreach ($row as $k => $v) {
                        $data[$headers[$k]] = $v;
                    }

                    $models[$data[$key]] = $data[$key];
                }
            }
        }

        if(count($models) > 1) {
            throw new Exception("Found more than one model: ", implode(", ", $models));
        }

        foreach ($models as $model) {
            $existing = AssessmentModel::where('name', $model)->first();
            if($existing) {
                print "Warning " . $model_prefix . " because it exists in the database\n";
            }
        }

        AssessmentSubCategory::importFromCSV(__DIR__ . "/" . $model_prefix . "_subcategory.csv", function ($row) {
            echo "Created sub category: " . $row->name . "\n";
        });

        AssessmentVariable::importFromCSV(__DIR__ . "/" . $model_prefix . "_variable.csv", function ($row) {
            echo "Created variable: " . $row->name . "\n";
        });

        AssessmentVariableValue::importFromCSV(__DIR__ . "/" . $model_prefix . "_variable_value.csv", function ($row) {
            echo "Created variable value: " . $row->name . "\n";
        });
    }
}
