<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!User::where('email', 'like','kelltrill@gmail.com')->first()) {
            User::create([
                'name' => "Kelly Quantrill",
                'email' => 'kelltrill@gmail.com',
                'password' => bcrypt('C0ntraWeir'),
            ]);
        } else {
            print "Skipping the existing Kelly user\n";
        }

        if (!User::where('email', 'like','claire.pattison@environment-agency.gov.uk')->first()) {
            User::create([
                'name' => "Claire Pattison",
                'email' => 'claire.pattison@environment-agency.gov.uk',
                'password' => bcrypt('C1@1r3We1r'),
            ]);
        } else {
            print "Skipping the existing Claire user\n";
        }

        if (!User::where('email', 'like','philip.rippon@environment-agency.gov.uk')->first()) {
            User::create([
                'name' => "Philip Rippon",
                'email' => 'philip.rippon@environment-agency.gov.uk',
                'password' => bcrypt('Ph1l1024'),
            ]);
        } else {
            print "Skipping the existing Philip user\n";
        }

        if (!User::where('email', 'like','robert.stephenson@environment-agency.gov.uk')->first()) {
            User::create([
                'name' => "Robert Stephenson",
                'email' => 'robert.stephenson@environment-agency.gov.uk',
                'password' => bcrypt('R0b31025'),
            ]);
        } else {
            print "Skipping the existing Robert user\n";
        }
    }
}
