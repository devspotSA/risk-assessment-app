
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Multiselect from 'vue-multiselect';

Vue.component(Multiselect);
Vue.component('risk-assessment-variable', require('./components/Risk-Assessment-Variable.vue'));
Vue.component('risk-assessment-sub-category-header', require('./components/Risk-Assessment-Sub-Category-Header.vue'));
Vue.component('risk-assessment-benefit', require('./components/Risk-Assessment-Benefit.vue'));

const app = new Vue({
    el: '#app'
});

jQuery(function() {

    jQuery('#risk-assessment-sub-category-header-container').affix({
        offset: {
            top: 60
        }
    });

    jQuery('#risk-assessment-sub-category-header-container').on('affixed.bs.affix', function () {
        jQuery(".container.risk-assessment-submit.affix + .container").css('padding-top',
            jQuery('#risk-assessment-sub-category-header-container').height() + 'px');
    });

    jQuery('#risk-assessment-sub-category-header-container').on('affixed-top.bs.affix', function () {
        jQuery(".container.risk-assessment-submit.affix-top + .container").css('padding-top', '0px');
    });
});