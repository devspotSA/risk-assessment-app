@extends('layouts.app')

@section('page-title', 'Create Category')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
        <li class="active">{{ link_to_route('assessmentModel.show', $parent->name, [$parent->id]) }}</li>
        <li>Create Category</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Create Category</h3>
                        @include('assessmentCategory._form', [ 'model' => new App\AssessmentCategory() ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection