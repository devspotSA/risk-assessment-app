@extends('layouts.app')

@section('page-title')
    {{ $model->name }}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
        <li class="active">{{ link_to_route('assessmentModel.show', $model->assessmentModel->name, [$model->assessmentModel->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentCategory.show', $model->name, [$model->id]) }}</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-right">
                            <a href="{{ route('assessmentCategory.edit',  [$model->id]) }}">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                        </div>
                        <dl class="dl-horizontal">
                           <dt>Category Name</dt>
                            <dd>{{ $model->name }}</dd>

                            <dt>Description</dt>
                            <dd>{{ $model->description }}</dd>

                            <dt>Image</dt>
                            <dd>{{ $model->image }}</dd>

                            <dt>Model</dt>
                            <dd>
                                <a href="{{ route('assessmentModel.show',  [$model->id]) }}">
                                    {{ $model->assessmentModel->name }}
                                </a>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>Sub Categories</h1>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>
                                        Order
                                    </th>
                                    <th colspan="2">
                                        Sub Category
                                    </th>
                                    <th>
                                        Category Weighting
                                    </th>
                                    <th>
                                        Assessment Weighting
                                    </th>
                                    <th>
                                        Ignoreable
                                    </th>
                                    <td align="right">
                                        <a href="{{ route('assessmentSubCategory.create') . '?parent=' . $model->id }}" class="pull-right">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </a>
                                    </td>
                                </tr>
                                @foreach($model->assessmentSubCategories()->orderBy('display_weight')->get() as $ix)
                                    <tr>
                                        <td>
                                            {{ $ix->display_weight }}
                                        </td>
                                        <td>
                                            <a href="{{ route('assessmentSubCategory.show',  [$ix->id]) }}">
                                                {{ $ix->name }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $ix->description }}
                                        </td>
                                        <td>
                                            {{ $ix->category_weighting }}
                                        </td>
                                        <td>
                                            {{ $ix->assessment_weighting }}
                                        </td>
                                        <td>
                                            @if($ix->ignoreable)
                                                Yes
                                            @else
                                                No
                                            @endif
                                        </td>
                                        <td width="10px">
                                            <a href="{{ route('assessmentSubCategory.edit',  [$ix->id]) }}">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection