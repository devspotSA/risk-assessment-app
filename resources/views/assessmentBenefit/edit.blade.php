@extends('layouts.app')

@section('page-title', 'Update Benefit')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
        <li class="active">{{ link_to_route('assessmentModel.show', $model->assessmentModel->name, [$model->assessmentModel->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentBenefit.show', $model->name, [$model->id]) }}</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('assessmentBenefit._form', compact('assessmentBenefit'))
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection