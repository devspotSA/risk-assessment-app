@extends('layouts.app')

@section('page-title', 'Create Benefit')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
        <li class="active">{{ link_to_route('assessmentModel.show', $parent->name, [$parent->id]) }}</li>
        <li>Create Benefit</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Create Benefit</h3>
                        @include('assessmentBenefit._form', [ 'model' => new App\AssessmentBenefit() ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection