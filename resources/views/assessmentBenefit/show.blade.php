@extends('layouts.app')

@section('page-title')
    {{ $model->name }}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
        <li class="active">{{ link_to_route('assessmentModel.show', $model->assessmentModel->name, [$model->assessmentModel->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentBenefit.show', $model->name, [$model->id]) }}</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-right">
                            <a href="{{ route('assessmentBenefit.edit',  [$model->id]) }}">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                        </div>
                        <dl class="dl-horizontal">
                           <dt>Benefit Name</dt>
                            <dd>{{ $model->name }}</dd>

                            <dt>Description</dt>
                            <dd>{{ $model->description }}</dd>

                            <dt>Image</dt>
                            <dd>{{ $model->image }}</dd>

                            <dt>Maximum value</dt>
                            <dd>{{ $model->maximum_value }}</dd>

                            <dt>Model</dt>
                            <dd>
                                <a href="{{ route('assessmentModel.show',  [$model->id]) }}">
                                    {{ $model->assessmentModel->name }}
                                </a>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection