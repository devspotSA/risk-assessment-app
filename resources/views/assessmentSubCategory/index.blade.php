@extends('layouts.app')

@section('page-title', 'SubCategories')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentSubCategory.index','SubCategories') }}</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        Dead End
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
