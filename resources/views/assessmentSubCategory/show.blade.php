@extends('layouts.app')

@section('page-title')
    {{ $model->name }}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
        <li class="active">{{ link_to_route('assessmentModel.show', $model->assessmentCategory->assessmentModel->name, [$model->assessmentCategory->assessmentModel->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentCategory.show', $model->assessmentCategory->name, [$model->assessmentCategory->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentSubCategory.show', $model->name, [$model->id]) }}</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-right">
                            <a href="{{ route('assessmentSubCategory.edit',  [$model->id]) }}">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                        </div>
                        <dl class="dl-horizontal">
                           <dt>Sub Category Name</dt>
                            <dd>{{ $model->name }}</dd>

                            <dt>Description</dt>
                            <dd>{{ $model->description }}</dd>

                            <dt>Category Weighting</dt>
                            <dd>{{ $model->category_weighting }}</dd>

                            <dt>Assessment Weighting</dt>
                            <dd>{{ $model->assessment_weighting }}</dd>

                            <dt>Ignoreable</dt>
                            <dd>
                                @if($model->ignoreable)
                                    Yes
                                @else
                                    No
                                @endif
                            </dd>

                            <dt>Image</dt>
                            <dd>{{ $model->image }}</dd>

                            <dt>Category</dt>
                            <dd>
                                <a href="{{ route('assessmentCategory.show',  [$model->assessmentCategory->id]) }}">
                                    {{ $model->assessmentCategory->name }}
                                </a>
                            </dd>

                            <dt>Model</dt>
                            <dd>
                                <a href="{{ route('assessmentModel.show',  [$model->assessmentCategory->assessmentModel->id]) }}">
                                    {{ $model->assessmentCategory->assessmentModel->name }}
                                </a>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>Variables</h1>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>
                                        Order
                                    </th>
                                    <th colspan="2">
                                        Variable
                                    </th>
                                    <th>
                                        Rank
                                    </th>
                                    <td align="right">
                                        <a href="{{ route('assessmentVariable.create') . '?parent=' . $model->id }}" class="pull-right">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </a>
                                    </td>
                                </tr>
                                @foreach($model->assessmentVariables()->orderBy('display_weight')->get() as $ix)
                                    <tr>
                                        <td>
                                            {{ $ix->display_weight }}
                                        </td>
                                        <td>
                                            <a href="{{ route('assessmentVariable.show',  [$ix->id]) }}">
                                                {{ $ix->name }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $ix->description }}
                                        </td>
                                        <td>
                                            {{ $ix->rank }}
                                        </td>
                                        <td width="10px">
                                            <a href="{{ route('assessmentVariable.edit',  [$ix->id]) }}">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection