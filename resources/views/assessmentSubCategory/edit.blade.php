@extends('layouts.app')

@section('page-title', 'Update SubCategory')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
        <li class="active">{{ link_to_route('assessmentModel.show', $model->assessmentCategory->assessmentModel->name, [$model->assessmentCategory->assessmentModel->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentCategory.show', $model->assessmentCategory->name, [$model->assessmentCategory->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentSubCategory.show', $model->name, [$model->id]) }}</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('assessmentSubCategory._form', compact('assessmentSubCategory'))
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection