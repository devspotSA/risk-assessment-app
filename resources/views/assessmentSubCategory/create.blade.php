@extends('layouts.app')

@section('page-title', 'Create SubCategory')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
        <li class="active">{{ link_to_route('assessmentModel.show', $parent->assessmentModel->name, [$parent->assessmentModel->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentCategory.show', $parent->name, [$parent->id]) }}</li>
        <li>Create Sub Category</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('assessmentSubCategory._form', [ 'model' => new App\AssessmentSubCategory() ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection