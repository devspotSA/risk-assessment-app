@if($model->id)
    {!! Form::model($model, ['route' => ['assessmentSubCategory.update', $model->id], 'method' => 'put']) !!}
@else
    {!! Form::model($model, ['route' => ['assessmentSubCategory.store']]) !!}
@endif

<div class="form-group {{ $errors->has('name') ? 'has-error' :'' }}">
    {{ Form::label('name') }}
    {{ Form::text('name', $model->name, ['class' => "form-control"]) }}
    {!! $errors->first('name','<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('description') ? 'has-error' :'' }}">
    {{ Form::label('description') }}
    {{ Form::text('description', $model->description, ['class' => "form-control"]) }}
    {!! $errors->first('description','<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('image') ? 'has-error' :'' }}">
    {{ Form::label('image') }}
    {{ Form::text('image', $model->image, ['class' => "form-control"]) }}
    {!! $errors->first('image','<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('display_weight') ? 'has-error' :'' }}">
    {{ Form::label('display_weight') }}
    {{ Form::number('display_weight', $model->display_weight, ['class' => "form-control"]) }}
    {!! $errors->first('display_weight','<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('category_weighting') ? 'has-error' :'' }}">
    {{ Form::label('category_weighting') }}
    {{ Form::text('category_weighting', $model->category_weighting, ['class' => "form-control"]) }}
    {!! $errors->first('category_weighting','<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('assessment_weighting') ? 'has-error' :'' }}">
    {{ Form::label('assessment_weighting') }}
    {{ Form::text('assessment_weighting', $model->assessment_weighting, ['class' => "form-control"]) }}
    {!! $errors->first('assessment_weighting','<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('ignoreable') ? 'has-error' :'' }}">
    {{ Form::label('ignoreable') }}
    {{ Form::checkbox('ignoreable', '1' , $model->ignoreable) }}
    {!! $errors->first('ignoreable','<span class="help-block">:message</span>') !!}
</div>

@if(! $model->id)
    {{Form::hidden('assessment_category_id', $parent->id)}}
@endif

<div class="form-group">
    {{ Form::submit('Save',['class' => 'btn btn-default']) }}
</div>

{!! Form::close() !!}