@extends('layouts.app')

@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li class="active">Home</li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            {{ $user->name }}'s Assessments
                            <p class="pull-right">
                            <a href="{{ url('/assessmentModel/') }}">
                                <span class="glyphicon glyphicon-zoom-in"></span> Explore Models
                            </a>
                            @if(Auth::user()->id == $user->id)
                                    &nbsp; | &nbsp;
                                <a href="{{ url('/assessmentSubmission/create') }}">
                                    <span class="glyphicon glyphicon-plus-sign"></span> Add Assessment
                                </a>
                            @endif

                            </p>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table home-assessments">
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Model</th>
                                <th>Risk Rating</th>
                            </tr>
                            @foreach($user->assessmentSubmissions as $submission)
                                <tr>
                                    <td>
                                        <a href="{{ url('/assessmentSubmission/' . $submission->id) }}">
                                            {{ $submission->name }}
                                        </a>
                                    </td>
                                    <td>
                                        {{ $submission->description }}
                                    </td>
                                    <td>
                                        {{ $submission->assessmentModel->name }}
                                    </td>
                                    <td class="assessment-rating-{{ $submission->getAssessmentRiskRatingLabelStyle() }}">
                                        {{ $submission->getAssessmentRiskRatingLabel() }}
                                    </td>
                                </tr>
                        @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
