@extends('layouts.app')

@section('page-title')
    {{ $model->name }}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
        <li class="active">{{ link_to_route('assessmentModel.show', $model->assessmentSubCategory->assessmentCategory->assessmentModel->name, [$model->assessmentSubCategory->assessmentCategory->assessmentModel->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentCategory.show', $model->assessmentSubCategory->assessmentCategory->name, [$model->assessmentSubCategory->assessmentCategory->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentSubCategory.show', $model->assessmentSubCategory->name, [$model->assessmentSubCategory->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentVariable.show', $model->name, [$model->id]) }}</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-right">
                            <a href="{{ route('assessmentVariable.edit',  [$model->id]) }}">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                        </div>
                        <dl class="dl-horizontal">
                           <dt>Variable Name</dt>
                            <dd>{{ $model->name }}</dd>

                            <dt>Description</dt>
                            <dd>{{ $model->description }}</dd>

                            <dt>Image</dt>
                            <dd>{{ $model->image }}</dd>

                            <dt>Rank</dt>
                            <dd>{{ $model->rank }}</dd>

                            <dt>Sub Category</dt>
                            <dd>
                                <a href="{{ route('assessmentSubCategory.show',  [$model->assessmentSubCategory->id]) }}">
                                    {{ $model->assessmentSubCategory->name }}
                                </a>
                            </dd>

                            <dt>Category</dt>
                            <dd>
                                <a href="{{ route('assessmentCategory.show',  [$model->assessmentSubCategory->assessmentCategory->id]) }}">
                                    {{ $model->assessmentSubCategory->assessmentCategory->name }}
                                </a>
                            </dd>

                            <dt>Model</dt>
                            <dd>
                                <a href="{{ route('assessmentModel.show',  [$model->assessmentSubCategory->assessmentCategory->assessmentModel->id]) }}">
                                    {{ $model->assessmentSubCategory->assessmentCategory->assessmentModel->name }}
                                </a>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>Variable Values</h1>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>
                                        Order
                                    </th>
                                    <th>
                                        Variable
                                    </th>
                                    <th>
                                        Value
                                    </th>
                                    <td align="right">
                                        <a href="{{ route('assessmentVariableValue.create') . '?parent=' . $model->id }}" class="pull-right">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </a>
                                    </td>
                                </tr>
                                @foreach($model->assessmentVariableValues()->orderBy('display_weight')->get() as $ix)
                                    <tr>
                                        <td>
                                            {{ $ix->display_weight }}
                                        </td>
                                        <td>
                                            <a href="{{ route('assessmentVariableValue.show',  [$ix->id]) }}">
                                                {{ $ix->name }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $ix->weighting }}
                                        </td>
                                        <td width="10px">
                                            <a href="{{ route('assessmentVariableValue.edit',  [$ix->id]) }}">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection