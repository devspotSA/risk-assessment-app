@extends('layouts.app')

@section('page-title', 'Create Variable')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
        <li class="active">{{ link_to_route('assessmentModel.show', $parent->assessmentCategory->assessmentModel->name, [$parent->assessmentCategory->assessmentModel->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentCategory.show', $parent->assessmentCategory->name, [$parent->assessmentCategory->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentSubCategory.show', $parent->name, [$parent->id]) }}</li>
        <li>Create Variabley</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('assessmentVariable._form', [ 'model' => new App\AssessmentVariable() ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection