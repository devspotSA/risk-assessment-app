@extends('layouts.app')

@section('page-title', 'Update Variable')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
        <li class="active">{{ link_to_route('assessmentModel.show', $model->assessmentSubCategory->assessmentCategory->assessmentModel->name, [$model->assessmentSubCategory->assessmentCategory->assessmentModel->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentCategory.show', $model->assessmentSubCategory->assessmentCategory->name, [$model->assessmentSubCategory->assessmentCategory->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentSubCategory.show', $model->assessmentSubCategory->name, [$model->assessmentSubCategory->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentVariable.show', $model->name, [$model->id]) }}</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('assessmentVariable._form', compact('assessmentVariable'))
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection