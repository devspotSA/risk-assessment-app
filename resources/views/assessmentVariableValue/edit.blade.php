@extends('layouts.app')

@section('page-title', 'Update VariableValue')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
        <li class="active">{{ link_to_route('assessmentModel.show', $model->assessmentVariable->assessmentSubCategory->assessmentCategory->assessmentModel->name, [$model->assessmentVariable->assessmentSubCategory->assessmentCategory->assessmentModel->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentCategory.show', $model->assessmentVariable->assessmentSubCategory->assessmentCategory->name, [$model->assessmentVariable->assessmentSubCategory->assessmentCategory->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentSubCategory.show', $model->assessmentVariable->assessmentSubCategory->name, [$model->assessmentVariable->assessmentSubCategory->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentVariable.show', $model->assessmentVariable->name, [$model->assessmentVariable->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentVariableValue.show', $model->name, [$model->id]) }}</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('assessmentVariableValue._form', compact('assessmentVariableValue'))
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection