@if($model->id)
    {!! Form::model($model, ['route' => ['assessmentVariableValue.update', $model->id], 'method' => 'put']) !!}
@else
    {!! Form::model($model, ['route' => ['assessmentVariableValue.store']]) !!}
@endif

<div class="form-group {{ $errors->has('name') ? 'has-error' :'' }}">
    {{ Form::label('name') }}
    {{ Form::text('name', $model->name, ['class' => "form-control"]) }}
    {!! $errors->first('name','<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('image') ? 'has-error' :'' }}">
    {{ Form::label('image') }}
    {{ Form::text('image', $model->image, ['class' => "form-control"]) }}
    {!! $errors->first('image','<span class="help-block">:message</span>') !!}
</div>


<div class="form-group {{ $errors->has('display_weight') ? 'has-error' :'' }}">
    {{ Form::label('display_weight') }}
    {{ Form::number('display_weight', $model->display_weight, ['class' => "form-control"]) }}
    {!! $errors->first('display_weight','<span class="help-block">:message</span>') !!}
</div>


<div class="form-group {{ $errors->has('weighting') ? 'has-error' :'' }}">
    {{ Form::label('weighting') }}
    {{ Form::number('weighting', $model->weighting, ['class' => "form-control"]) }}
    {!! $errors->first('weighting','<span class="help-block">:message</span>') !!}
</div>

@if(! $model->id)
    {{Form::hidden('assessment_variable_id', $parent->id)}}
@endif

<div class="form-group">
    {{ Form::submit('Save',['class' => 'btn btn-default']) }}
</div>

{!! Form::close() !!}