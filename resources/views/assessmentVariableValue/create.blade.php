@extends('layouts.app')

@section('page-title', 'Create Variable Value')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
        <li class="active">{{ link_to_route('assessmentModel.show', $parent->assessmentSubCategory->assessmentCategory->assessmentModel->name, [$parent->assessmentSubCategory->assessmentCategory->assessmentModel->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentCategory.show', $parent->assessmentSubCategory->assessmentCategory->name, [$parent->assessmentSubCategory->assessmentCategory->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentSubCategory.show', $parent->assessmentSubCategory->name, [$parent->assessmentSubCategory->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentVariable.show', $parent->name, [$parent->id]) }}</li>
        <li>Create Variable Value</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('assessmentVariableValue._form', [ 'model' => new App\AssessmentVariableValue() ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection