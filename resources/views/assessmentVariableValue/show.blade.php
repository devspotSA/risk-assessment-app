@extends('layouts.app')

@section('page-title')
    {{ $model->name }}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
        <li class="active">{{ link_to_route('assessmentModel.show', $model->assessmentVariable->assessmentSubCategory->assessmentCategory->assessmentModel->name, [$model->assessmentVariable->assessmentSubCategory->assessmentCategory->assessmentModel->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentCategory.show', $model->assessmentVariable->assessmentSubCategory->assessmentCategory->name, [$model->assessmentVariable->assessmentSubCategory->assessmentCategory->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentSubCategory.show', $model->assessmentVariable->assessmentSubCategory->name, [$model->assessmentVariable->assessmentSubCategory->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentVariable.show', $model->assessmentVariable->name, [$model->assessmentVariable->id]) }}</li>
        <li class="active">{{ link_to_route('assessmentVariableValue.show', $model->name, [$model->id]) }}</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-right">
                            <a href="{{ route('assessmentVariableValue.edit',  [$model->id]) }}">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                        </div>
                        <dl class="dl-horizontal">
                           <dt>VariableValue Name</dt>
                            <dd>{{ $model->name }}</dd>

                            <dt>Image</dt>
                            <dd>{{ $model->image }}</dd>

                            <dt>Weighting</dt>
                            <dd>{{ $model->weighting }}</dd>

                            <dt>Variable</dt>
                            <dd>
                                <a href="{{ route('assessmentVariable.show',  [$model->assessmentVariable->id]) }}">
                                    {{ $model->assessmentVariable->name }}
                                </a>
                            </dd>

                            <dt>Sub Category</dt>
                            <dd>
                                <a href="{{ route('assessmentSubCategory.show',  [$model->assessmentVariable->assessmentSubCategory->id]) }}">
                                    {{ $model->assessmentVariable->assessmentSubCategory->name }}
                                </a>
                            </dd>

                            <dt>Category</dt>
                            <dd>
                                <a href="{{ route('assessmentCategory.show',  [$model->assessmentVariable->assessmentSubCategory->assessmentCategory->id]) }}">
                                    {{ $model->assessmentVariable->assessmentSubCategory->assessmentCategory->name }}
                                </a>
                            </dd>

                            <dt>Model</dt>
                            <dd>
                                <a href="{{ route('assessmentModel.show',  [$model->assessmentVariable->assessmentSubCategory->assessmentCategory->assessmentModel->id]) }}">
                                    {{ $model->assessmentVariable->assessmentSubCategory->assessmentCategory->assessmentModel->name }}
                                </a>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection