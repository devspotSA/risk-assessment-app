@extends('layouts.app')

@section('page-title')
    {{ $model->name }}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
        <li class="active">{{ link_to_route('assessmentModel.show', $model->name, [$model->id]) }}</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-right">
                            <a href="{{ route('assessmentModel.edit',  [$model->id]) }}">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                        </div>
                        <dl class="dl-horizontal">
                            <dt>Model Name</dt>
                            <dd>{{ $model->name }}</dd>

                            <dt>Description</dt>
                            <dd>{{ $model->description }}</dd>

                            <dt>Image</dt>
                            <dd>{{ $model->image }}</dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>Categories</h1>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>
                                        Order
                                    </th>
                                    <th colspan="2">
                                        Category
                                    </th>
                                    <td align="right">
                                        <a href="{{ route('assessmentCategory.create' )  . '?parent=' . $model->id  }}" class="pull-right">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </a>
                                    </td>
                                </tr>
                                @foreach($model->assessmentCategories()->orderBy('display_weight')->get() as $ix)
                                    <tr>
                                        <td>
                                            {{ $ix->display_weight }}
                                        </td>
                                        <td>
                                            <a href="{{ route('assessmentCategory.show',  [$ix->id]) }}">
                                                {{ $ix->name }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $ix->description }}
                                        </td>
                                        <td width="10px">
                                            <a href="{{ route('assessmentCategory.edit',  [$ix->id]) }}">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>Benefits</h1>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>
                                        Order
                                    </th>
                                    <th colspan="2">
                                        Benefit
                                    </th>
                                    <th>
                                        Maximum Value
                                    </th>
                                    <td align="right">
                                        <a href="{{ route('assessmentBenefit.create' )  . '?parent=' . $model->id  }}" class="pull-right">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </a>
                                    </td>
                                </tr>
                                @foreach($model->assessmentBenefits()->orderBy('display_weight')->get() as $ix)
                                    <tr>
                                        <td>
                                            {{ $ix->display_weight }}
                                        </td>
                                        <td>
                                            <a href="{{ route('assessmentBenefit.show',  [$ix->id]) }}">
                                                {{ $ix->name }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $ix->description }}
                                        </td>
                                        <td>
                                            {{ $ix->maximum_value }}
                                        </td>
                                        <td width="10px">
                                            <a href="{{ route('assessmentBenefit.edit',  [$ix->id]) }}">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection