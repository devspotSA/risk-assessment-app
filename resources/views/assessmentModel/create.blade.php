@extends('layouts.app')

@section('page-title', 'Create Model')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('assessmentModel._form', [ 'model' => new App\AssessmentModel() ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection