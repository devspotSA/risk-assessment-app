@extends('layouts.app')

@section('page-title')
    {{ $model->name }}
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2>
                            Model: {{ $model->name }}
                        </h2>
                        <p>
                            <b>Hierarchy:</b> Category > Subcategory > Variable > Variable Value
                        </p>
                        <p>
                            Subcategories are weighted relevant to other subcategories within each category as well as
                            weighted globally relevant to all other subcategories.
                            Variables are ranked according to one another and variable values have vulnerability
                            weightings, listed in order of severity.
                        </p>
                        <hr>
                        <p>
                            <b>Subcategory Risk</b> = ( R<sub>var1</sub> * W<sub>var1</sub> * KW<sub>var1</sub> ) + ( R<sub>var2</sub> * W<sub>var2</sub> * KW<sub>var2</sub>... )
                            <br>where<br>
                            R = Variable Rank<br>
                            W = Variable Value Vulnerability Weighting<br>
                            KW = Knowledge Weighting<br>
                        </p>
                        <hr>
                        <p>
                            <b>Knowledge Weightings (KW) are user input values:</b>
                        </p>
                        <table class="table">
                            <tr>
                                <td width="10%">1</td>
                                <td>Certain</td>
                            </tr>
                            <tr>
                                <td>1.5</td>
                                <td>Somewhat Certain</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Uncertain</td>
                            </tr>
                        </table>
                        <p>
                            Knowledge weightings are defaulted to 1 ("certain") in the "very familiar / familiar" model with no options to adjust certainty levels mid-assessment. Knowledge weightings are defaulted to 1.5 ("somewhat certain") in the "somewhat familiar / unfamiliar" model with options to adjust the certainty at the variable level throughout the assessment.
                        </p>
                        <hr>
                        <p>
                            Risk Levels (RL) are the same for subcategories, categories, and the total risk rating.
                        </p>
                        <table class="table">
                            <tr>
                                <td width="10%">5</td>
                                <td>Very High</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>High</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Moderate</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Low</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Very Low</td>
                            </tr>
                        </table>
                        <hr>
                        <p>
                            <b>Category Risk</b> = ( RL<sub>subcat1</sub> * CW<sub>subcat1</sub> ) + ( RL<sub>subcat2</sub> * CW<sub>subcat2</sub>... )
                            <br>where
                            <b>RL</b> = Subcategory Risk Level (scale of 1 - 5)<br>
                            <b>CW</b> = The subcategory weighting within the category (Internal Category Weighting)
                        </p>
                        <hr>
                        <p>
                            <b>Total Site Risk</b> = ( RL<sub>subcat1</sub> * GW<sub>subcat1</sub> ) + ( RL<sub>subcat2</sub> * GW<sub>subcat2</sub>...)
                            <br>where
                            <br><b>RL</b> = Subcategory Risk Level (scale of 1 - 5)
                            <br><b>GW</b> = Global Subcategory Weighting in relation to all other subcategories
                            regardless of
                            category
                        </p>
                        <hr>
                        <p>
                            Benefits are calculated independently
                        </p>
                        <table class="table">
                            <tr>
                                <td width="10%">
                                    Very High
                                </td>
                                <td>
                                    maximum value >= 7 & count > 1
                                </td>
                            </tr>
                            <tr>
                                <td>
                                     High
                                </td>
                                <td>
                                    maximum value >= 7
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Moderate
                                </td>
                                <td>
                                    4 <= maximum value <= 6
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Low
                                </td>
                                <td>
                                    2 <= maximum value <= 3
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Very Low
                                </td>
                                <td>
                                    maximum value <= 1
                                </td>
                            </tr>
                        </table>
                        <hr>
                        <p>
                            Subcategories defaulted to "Not Applicable", visible in grey when you start a new
                            assessment, are flagged as ignorable when no data is entered. This means their risk ratings
                            are not included in the category and total risk calculations.
                        </p>
                        <p>
                            Subcategories with a "Very Low Risk", equating to a Risk Value of 1, are not multiplied by
                            the CW or GW in the category and total risk calculations.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        @foreach($model->assessmentCategories()->orderBy('display_weight')->get() as $category)
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Category {{ $category->name }}
                        </div>
                        <div class="panel-body">
                            @foreach($category->assessmentSubCategories()->orderBy('display_weight')->get() as $subCategory)
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Subcategory: {{ $subCategory->name }}
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <b>Internal Subcategory
                                                                        Weighting:</b> {{ $subCategory->category_weighting }}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>Global Subcategory
                                                                        Weighting:</b> {{ $subCategory->assessment_weighting }}
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                                <table class="table">
                                                                    <tr>
                                                                        <th>Variable</th>
                                                                        <th>Rank</th>
                                                                        <th>Values</th>
                                                                    </tr>
                                                                    @foreach($subCategory->assessmentVariables()->orderBy('display_weight')->get() as $variable)
                                                                        <tr>
                                                                            <td>
                                                                                {{ $variable->name }}
                                                                            </td>
                                                                            <td>
                                                                                {{ $variable->rank }}
                                                                            </td>
                                                                            <td width="90%">
                                                                                <table class="table" width="100%">
                                                                                    @foreach($variable->assessmentVariableValues()->orderBy('display_weight')->get() as $value)
                                                                                        <tr>
                                                                                            <td>
                                                                                                {{ $value->name }}
                                                                                            </td>
                                                                                            <td width="5%">
                                                                                                {{ $value->weighting }}
                                                                                            </td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection