@extends('layouts.app')

@section('page-title', 'Models')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>{{ link_to_route('home','Home') }}</li>
        <li>{{ link_to_route('assessmentModel.index','Models') }}</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th colspan="2">
                                        Model
                                    </th>
                                    <td align="right">
                                        @if(Auth::user()->isAdmin())
                                          <a href="{{ route('assessmentModel.create') }}" class="pull-right">
                                            <span class="glyphicon glyphicon-plus"></span>
                                          </a>
                                        @endif
                                    </td>
                                </tr>
                                @foreach($models as $ix)
                                    <tr>
                                        <td>
                                            @if(Auth::user()->isAdmin())
                                              <a href="{{ route('assessmentModel.show',  [$ix->id]) }}">
                                                {{ $ix->name }}
                                              </a>
                                                <small>
                                                    <a href="{{ route('methodology',  [$ix->id]) }}">
                                                        <span class="label label-default pull-right">
                                                            methodology
                                                        </span>
                                                    </a>
                                                </small>
                                            @else
                                                <a href="{{ route('methodology',  [$ix->id]) }}">
                                                    {{ $ix->name }}
                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $ix->description }}
                                        </td>
                                        <td width="10px">
                                            @if(Auth::user()->isAdmin())
                                              <a href="{{ route('assessmentModel.edit',  [$ix->id]) }}">
                                                <span class="glyphicon glyphicon-edit"></span>
                                              </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-right">
                                        {{ $models->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
