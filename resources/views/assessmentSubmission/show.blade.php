@extends('layouts.app')

@section('content')
    <div class="container risk-assessment-show">
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}">
                    Home
                </a>
            </li>
            <li class="active">Risk Assessment: {{ $assessmentSubmission->name }}</li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                    <div class="alert alert-warning alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div>
                            <span class="glyphicon glyphicon-warning-sign"></span>
                            Once created, an assessment submission takes on average <strong>45 minutes</strong> to complete.
                        </div>
                        <div>
                            <span class="glyphicon glyphicon-save"></span>
                            Your answers to assessment questions are automatically saved.
                        </div>
                    </div>
                <div class="jumbotron">
                    <a href="{{ route('assessmentSubmission.edit', [$assessmentSubmission->id]) }}" class="pull-right">
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                    <h1>{{ $assessmentSubmission->name }}</h1>
                    <p>{{ $assessmentSubmission->description }}</p>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 assessment-rating-{{ $assessmentSubmission->getAssessmentRiskRatingLabelStyle() }}" align="center">
                            <table width="70%">
                                <tr>
                                    <th colspan="3">
                                        <h3>Risk Rating</h3>
                                    </th>
                                </tr>
                                <tr class="risk-level-result-very-high">
                                    <td width="15px">
                                        <span class="glyphicon glyphicon-stop risk-swatch"></span>
                                    </td>
                                    <td width="25px" class="text-center" align="center">
                                        <div class="glyphicon glyphicon-arrow-left risk-arrow"></div>
                                    </td>
                                    <td>
                                        <div class="risk-result-text-box">
                                            Very High Risk
                                        </div>
                                    </td>
                                </tr>
                                <tr class="risk-level-result-high">
                                    <td>
                                        <span class="glyphicon glyphicon-stop risk-swatch"></span>
                                    </td>
                                    <td width="25px" class="text-center" align="center">
                                        <div class="glyphicon glyphicon-arrow-left risk-arrow"></div>
                                    </td>
                                    <td>
                                        <div class="risk-result-text-box">
                                            High Risk
                                        </div>
                                    </td>
                                </tr>
                                <tr class="risk-level-result-moderate">
                                    <td>
                                        <span class="glyphicon glyphicon-stop risk-swatch"></span>
                                    </td>
                                    <td width="25px" class="text-center" align="center">
                                        <div class="glyphicon glyphicon-arrow-left risk-arrow"></div>
                                    </td>
                                    <td>
                                        <div class="risk-result-text-box">
                                            Moderate Risk
                                        </div>
                                    </td>
                                </tr>
                                <tr class="risk-level-result-low">
                                    <td>
                                        <span class="glyphicon glyphicon-stop risk-swatch"></span>
                                    </td>
                                    <td width="25px" class="text-center" align="center">
                                        <div class="glyphicon glyphicon-arrow-left risk-arrow"></div>
                                    </td>
                                    <td>
                                        <div class="risk-result-text-box">
                                            Low Risk
                                        </div>
                                    </td>
                                </tr>
                                <tr class="risk-level-result-very-low">
                                    <td>
                                        <span class="glyphicon glyphicon-stop risk-swatch"></span>
                                    </td>
                                    <td width="25px" class="text-center" align="center">
                                        <div class="glyphicon glyphicon-arrow-left risk-arrow"></div>
                                    </td>
                                    <td>
                                        <div class="risk-result-text-box">
                                            Very Low Risk
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6 assessment-rating-{{ $assessmentSubmission->getAssessmentBenefitRatingLabelStyle() }}" align="center">
                            <table width="70%">
                                <tr>
                                    <th colspan="3">
                                        <h3>Expected Benefits</h3>
                                    </th>
                                </tr>
                                <tr class="benefit-level-result-very-high">
                                    <td width="15px">
                                        <span class="glyphicon glyphicon-stop benefit-swatch"></span>
                                    </td>
                                    <td width="25px" class="text-center" align="center">
                                        <div class="glyphicon glyphicon-arrow-left benefit-arrow"></div>
                                    </td>
                                    <td>
                                        <div class="benefit-result-text-box">
                                            Very High Benefit(s)
                                        </div>
                                    </td>
                                </tr>
                                <tr class="benefit-level-result-high">
                                    <td>
                                        <span class="glyphicon glyphicon-stop benefit-swatch"></span>
                                    </td>
                                    <td width="25px" class="text-center" align="center">
                                        <div class="glyphicon glyphicon-arrow-left benefit-arrow"></div>
                                    </td>
                                    <td>
                                        <div class="benefit-result-text-box">
                                            High Benefit(s)
                                        </div>
                                    </td>
                                </tr>
                                <tr class="benefit-level-result-moderate">
                                    <td>
                                        <span class="glyphicon glyphicon-stop benefit-swatch"></span>
                                    </td>
                                    <td width="25px" class="text-center" align="center">
                                        <div class="glyphicon glyphicon-arrow-left benefit-arrow"></div>
                                    </td>
                                    <td>
                                        <div class="benefit-result-text-box">
                                            Moderate Benefit(s)
                                        </div>
                                    </td>
                                </tr>
                                <tr class="benefit-level-result-low">
                                    <td>
                                        <span class="glyphicon glyphicon-stop benefit-swatch"></span>
                                    </td>
                                    <td width="25px" class="text-center" align="center">
                                        <div class="glyphicon glyphicon-arrow-left benefit-arrow"></div>
                                    </td>
                                    <td>
                                        <div class="benefit-result-text-box">
                                            Low Benefit(s)
                                        </div>
                                    </td>
                                </tr>
                                <tr class="benefit-level-result-very-low">
                                    <td>
                                        <span class="glyphicon glyphicon-stop benefit-swatch"></span>
                                    </td>
                                    <td width="25px" class="text-center" align="center">
                                        <div class="glyphicon glyphicon-arrow-left benefit-arrow"></div>
                                    </td>
                                    <td>
                                        <div class="benefit-result-text-box">
                                            Very Low Benefit(s)
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @foreach($assessmentSubmission->assessmentModel->assessmentCategories as $category)
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default assessment-category-rating-{{ $assessmentSubmission->getCategoryRiskRatingLabelStyle($category) }}">
                        <div class="panel-heading">
                            <a id="cat{{ $category->id }}"></a>
                            <h3 class="panel-title">{{ $category->name }}
                                <span class="pull-right">
                                {{ $assessmentSubmission->getCategoryRiskRatingLabel($category) }}
                              </span>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="well well-sm">
                                        {{ $category->description }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                @foreach($category->assessmentSubCategories()->orderBy('display_weight')->get() as $subCategory)
                                    <div class="col-md-3">
                                        <div class="panel panel-default subcatbox assessment-sub-category-rating-{{ $assessmentSubmission->getSubCategoryRiskRatingLabelStyle($subCategory) }}">
                                            <div class="panel-heading">
                                                <a href="{{ route('submit', [
                                                            'assessmentSubmission' => $assessmentSubmission->id,
                                                            'assessmentSubCategory' => $subCategory->id
                                                        ]) }}">
                                                    <h3 class="panel-title">
                                                        {{ $subCategory->name }}
                                                        <span class="glyphicon glyphicon-th pull-right"></span>
                                                    </h3>
                                                </a>
                                            </div>

                                            <div class="panel-body text-center">
                                                <a href="{{ route('submit', [
                                                            'assessmentSubmission' => $assessmentSubmission->id,
                                                            'assessmentSubCategory' => $subCategory->id
                                                        ]) }}">
                                                    {{ $assessmentSubmission->getSubCategoryRiskRatingLabel($subCategory) }}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a id=benefits href="{{ route('submitBenefits', [
                                                            'assessmentSubmission' => $assessmentSubmission->id,
                                                        ]) }}">
                            <h3 class="panel-title">
                                Benefits
                                <span class="glyphicon glyphicon-th pull-right"></span>
                            </h3>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="well well-sm">
                                    These are the areas that you expect will improve as a direct or indirect result of the removal project.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <table class="table">
                                <tr>
                                    <th>
                                        &nbsp;
                                    </th>
                                    <th>
                                        <table class="table">
                                            <th class="text-left" width="33%">
                                                None
                                            </th>
                                            <th class="text-center" width="33%">
                                                Moderate
                                            </th>
                                            <th class="text-right" width="33%">
                                                Significant
                                            </th>
                                        </table>
                                    </th>
                                    <th>
                                       &nbsp;
                                    </th>
                                </tr>
                                @foreach($assessmentSubmission->assessmentModel->assessmentBenefits()->orderBy('display_weight')->get() as $assessmentBenefit)
                                    <tr>
                                        <td>
                                            <a id=benefits href="{{ route('submitBenefits', [
                                                            'assessmentSubmission' => $assessmentSubmission->id,
                                                        ]) }}">
                                                {{ $assessmentBenefit->name }}
                                            </a>
                                        </td>
                                        <td width="60%">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info"
                                                     role="progressbar"
                                                     aria-valuenow=""
                                                     aria-valuemin="0"
                                                     aria-valuemax="{{ $assessmentSubmission->getAssessmentBenefitSubmissionValue($assessmentBenefit) }}"
                                                     style="width: {{ $assessmentSubmission->getAssessmentBenefitSubmissionValuePercent($assessmentBenefit) }}%">
                                                    <span class="sr-only">{{ $assessmentSubmission->getAssessmentBenefitSubmissionValuePercent($assessmentBenefit) }}
                                                        %</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <a id=benefits href="{{ route('submitBenefits', [
                                                            'assessmentSubmission' => $assessmentSubmission->id,
                                                        ]) }}">
                                                <span class="glyphicon glyphicon-th pull-right"></span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
