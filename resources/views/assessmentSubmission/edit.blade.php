@extends('layouts.app')

@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}">
                    Home
                </a>
            </li>
            <li class="active">Update Assessment</li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                    <h1>Update Assessment</h1>
                    <p>Use the form below to update an existing assessment. You cannot change the model for an
                        assessment once created.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-12">
                            @include('assessmentSubmission._form', [ 'assessmentSubmission' => $assessmentSubmission, 'user' => $assessmentSubmission->user ])
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
