@extends('layouts.app')

@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}">
                    Home
                </a>
            </li>
            <li class="active">Create Assessment</li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                    <h1>New Assessment</h1>
                    <p>Use the form below to setup a new assessment based on an existing model</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('assessmentSubmission._form', [ 'assessmentSubmission' => new \App\AssessmentSubmission(), 'user' => $user ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
