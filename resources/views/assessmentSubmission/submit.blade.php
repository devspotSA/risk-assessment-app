@extends('layouts.app')

@section('content')
    <div class="container risk-assessment-submit">
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/assessmentSubmission' , [$assessmentSubmission->id]) }}">
                    {{ $assessmentSubCategory->assessmentCategory->assessmentModel->name }}
                </a>
            </li>
            <li>
                <a href="{{ url('assessmentSubmission' , [$assessmentSubmission->id]) }}#cat{{ $assessmentSubCategory->assessmentCategory->id}}">
                    {{ $assessmentSubCategory->assessmentCategory->name }}
                </a>
            </li>
            <li class="active">{{ $assessmentSubCategory->name }}</li>
        </ol>
    </div>

    <div class="container risk-assessment-submit" id="risk-assessment-sub-category-header-container">
        <div class="row">
            <div class="col-md-12">
                <risk-assessment-sub-category-header assessment-submission-id="{{ $assessmentSubmission->id }}"
                                                     sub-category-id="{{ $assessmentSubCategory->id }}"
                                                     api-token="{{ Auth::user()->api_token }}"/>

            </div>
        </div>
    </div>

    <div class="container risk-assessment-submit">
        @foreach($assessmentSubCategory->assessmentVariables()->orderBy('display_weight')->get() as $assessmentVariable)
            <div class="row">
                <div class="col-md-12">
                    <risk-assessment-variable assessment-submission-id="{{ $assessmentSubmission->id }}"
                                              variable-id="{{ $assessmentVariable->id }}"
                                              api-token="{{ Auth::user()->api_token }}"/>
                </div>
            </div>
        @endforeach

        <div class="row">
            <div class="col-md-12">
                <nav aria-label="">
                    <ul class="pager">
                        @if( $assessmentSubmission->prevSubmit($assessmentSubCategory) )
                            <li class="previous"><a
                                        href="{{ url('/submit/' . $assessmentSubmission->id . '/' . $assessmentSubmission->prevSubmit($assessmentSubCategory) ) }}"><span
                                            aria-hidden="true">&larr;</span> {{ \App\AssessmentSubCategory::find($assessmentSubmission->prevSubmit($assessmentSubCategory))->name }}
                                </a></li>
                        @endif

                        @if( $assessmentSubmission->nextSubmit($assessmentSubCategory) )
                            <li class="next"><a
                                        href="{{ url('/submit/' . $assessmentSubmission->id . '/' . $assessmentSubmission->nextSubmit($assessmentSubCategory) ) }}">{{ \App\AssessmentSubCategory::find($assessmentSubmission->nextSubmit($assessmentSubCategory))->name }}
                                    <span aria-hidden="true">&rarr;</span></a></li>
                        @else
                            <li class="next"><a href="{{ url('/submitBenefits' , [$assessmentSubmission->id]) }}">
                                    Benefits <span aria-hidden="true">&rarr;</span></a></li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>
@endsection
