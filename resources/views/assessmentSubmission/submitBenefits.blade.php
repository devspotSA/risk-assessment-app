@extends('layouts.app')

@section('content')
    <div class="container risk-assessment-submit">
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/assessmentSubmission' , [$assessmentSubmission->id]) }}">
                    {{ $assessmentSubmission->assessmentModel->name }}
                </a>
            </li>
            <li class="active">Benefits</li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <div class="well well-sm">
                            Which of these areas do you expect will improve as a direct or indirect result of the
                            removal project?
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <p>
                    &nbsp;
                </p>
            </div>
            <div class="col-md-6">
                <table class="table">
                    <tr>
                        <td width="33%">
                            <span class="pull-left">None</span>
                        </td>
                        <td class="text-center" width="33%">
                            <span class="text-center">Moderate</span>
                        </td>
                        <td width="33%">
                            <span class="pull-right">Significant</span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-2">
                &nbsp;
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        @foreach($assessmentSubmission->assessmentModel->assessmentBenefits as $assessmentBenefit)
                            <div>
                                <risk-assessment-benefit assessment-submission-id="{{ $assessmentSubmission->id }}"
                                                         benefit-id="{{ $assessmentBenefit->id }}"
                                                         api-token="{{ Auth::user()->api_token }}"/>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="...">
                    <ul class="pager">
                        @if( $assessmentSubmission->finalSubmit() )
                            <li class="previous"><a href="{{ url('/submit/' . $assessmentSubmission->id . '/' . $assessmentSubmission->finalSubmit() ) }}"><span aria-hidden="true">&larr;</span> {{ \App\AssessmentSubCategory::find($assessmentSubmission->finalSubmit())->name }}</a></li>
                        @endif


                        <li class="next"> <a href="{{ url('/assessmentSubmission' , [$assessmentSubmission->id]) }}">Complete <span aria-hidden="true">&rarr;</span></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
@endsection
