@if($assessmentSubmission->id)
    <h2>Update Assessment {{ $assessmentSubmission->name }}</h2>
    {!! Form::model($assessmentSubmission, ['route' => ['assessmentSubmission.update', $assessmentSubmission->id], 'method' => 'put']) !!}
@else
    <h3>Create Assessment</h3>
    {!! Form::model($assessmentSubmission, ['route' => ['assessmentSubmission.store']]) !!}
@endif

<div class="col-md-12">
    <div class="alert alert-warning">
        <div>
            <span class="glyphicon glyphicon-warning-sign"></span>
            Once created, an assessment submission takes on average <strong>45 minutes</strong> to complete.
        </div>
        <div>
            <span class="glyphicon glyphicon-save"></span>
            Your answers to assessment questions are automatically saved.
        </div>

    </div>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' :'' }}">
    {{ Form::label('name') }}
    {{ Form::text('name', $assessmentSubmission->name, ['class' => "form-control"]) }}
    {!! $errors->first('name','<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' :'' }}">
    {{ Form::label('description') }}
    {{ Form::text('description', $assessmentSubmission->description, ['class' => "form-control"]) }}
    {!! $errors->first('description','<span class="help-block">:message</span>') !!}
</div>

@if(! $assessmentSubmission->id)
    <div class="form-group {{ $errors->has('assessment_model_id') ? 'has-error' :'' }}">
        {{ Form::label('assessment_model_id', 'Assessment Model') }}
        <div class="alert alert-info">
            <div>
                The model you select here can not be changed once you start your assessment.
                The models prioritise different categories in the tool. For example, if you are aware that there is a high
                volume of infrastructure in the area then select the "Infrastructure High" model. If you are uncertain of
                which one to choose then leave it set to the default.
            </div>
        </div>
        {{ Form::select('assessment_model_id', \App\AssessmentModel::pluck('name','id'), '', ['class' => "form-control"]) }}
        {!! $errors->first('assessment_model_id','<span class="help-block">:message</span>') !!}
    </div>
    <div class="form-group {{ $errors->has('site_knowledge') ? 'has-error' :'' }}">
        {{ Form::label('site_knowledge', 'How familiar are you with the site you are assessing?') }}
        <div class="alert alert-info">
            <div>
                This can not be changed once the assessment has been started.
            </div>
        </div>
        {{ Form::select('site_knowledge', \App\AssessmentSubmission::SITE_KNOWLEDGE_OPTIONS, '', ['class' => "form-control"]) }}
        {!! $errors->first('site_knowledge','<span class="help-block">:message</span>') !!}
    </div>
    {{ Form::hidden('user_id', $user->id) }}
@endif

<div class="form-group">
    {{ Form::submit('Save',['class' => 'btn btn-default']) }}
</div>

{!! Form::close() !!}