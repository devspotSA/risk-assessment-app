<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssessmentSubmissionVariableSelection extends Model
{
    protected $fillable = [
        'assessment_submission_id',
        'assessment_variable_id',
        'assessment_variable_value_id',
        'knowledge_weighting'
    ];

    public function assessmentSubmission() {
        return $this->belongsTo(AssessmentSubmission::class);
    }

    public function assessmentVariable() {
        return $this->belongsTo(AssessmentVariable::class);
    }

    public function assessmentVariableValue() {
        return $this->belongsTo( AssessmentVariableValue::class);
    }
}
