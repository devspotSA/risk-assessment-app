<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Closure;

class AssessmentCategory extends Model
{
    protected $fillable = [
        'name',
        'description',
        'image',
        'assessment_model_id',
        'display_weight',
    ];

    public function assessmentModel()
    {
        return $this->belongsTo(AssessmentModel::class);
    }

    public function assessmentSubCategories()
    {
        return $this->hasMany(AssessmentSubCategory::class);
    }

    public static function importFromCSV($source_file, Closure $callback)
    {
        if (!file_exists($source_file)) {
            throw new \Exception("File not found: " . $source_file);
        }

        $row_num = 0;
        $headers = [];
        if (($handle = fopen($source_file, "r")) !== FALSE) {
            while (($row = fgetcsv($handle, 4096, ",")) !== FALSE) {
                $row_num++;
                if ($row_num == 1) {
                    $headers = $row;
                    continue;
                }

                $data = [];
                foreach ($row as $k => $v) {
                    $data[$headers[$k]] = $v;
                }

                try {
                    $assessmentModel = assessmentModel::where('name', $data['model'])->first();
                    unset($data['model']);

                    $newRow = $assessmentModel->assessmentCategories()->create($data);
                } catch (\Exception $ex) {
                    print "Shit: Died at " . print_r($data, TRUE);
                    throw $ex;
                }

                $callback($newRow);
            }

            fclose($handle);

            // Account for the header
            return ($row_num - 1);
        }
    }
}
