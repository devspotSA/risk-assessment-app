<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class AssessmentSubmission extends Model
{
    protected $fillable = [
        'name',
        'description',
        'site_knowledge',
        'assessment_model_id'
    ];

    const SITE_KNOWLEDGE_WELL_KNOWN = 1;
    const SITE_KNOWLEDGE_WELL_KNOWN_LABEL = "Very familiar / Familiar";

    const SITE_KNOWLEDGE_NOT_WELL_KNOWN = 2;
    const SITE_KNOWLEDGE_NOT_WELL_KNOWN_LABEL = "Somewhat familiar / Unfamiliar";

    const SITE_KNOWLEDGE_OPTIONS = [
        self::SITE_KNOWLEDGE_WELL_KNOWN => self::SITE_KNOWLEDGE_WELL_KNOWN_LABEL,
        self::SITE_KNOWLEDGE_NOT_WELL_KNOWN => self::SITE_KNOWLEDGE_NOT_WELL_KNOWN_LABEL
    ];

    const MAX_KNOWLEDGE_WEIGHTING = 2;

    const BAND_VERY_LOW = 1;
    const BAND_LOW = 2;
    const BAND_MODERATE = 3;
    const BAND_HIGH = 4;
    const BAND_VERY_HIGH = 5;

    const RISK_BANDS = [
        self::BAND_VERY_LOW => self::BAND_VERY_LOW,
        self::BAND_LOW => self::BAND_LOW,
        self::BAND_MODERATE => self::BAND_MODERATE,
        self::BAND_HIGH => self::BAND_HIGH,
        self::BAND_VERY_HIGH => self::BAND_VERY_HIGH
    ];

    const RISK_BANDS_LABELS = [
        self::BAND_VERY_LOW => "Very Low Risk",
        self::BAND_LOW => "Low Risk",
        self::BAND_MODERATE => "Moderate Risk",
        self::BAND_HIGH => "High Risk",
        self::BAND_VERY_HIGH => "Very High Risk"
    ];


    const BAND_NO_BENEFITS = 0;
    const BAND_BEN_VERY_LOW = 1;
    const BAND_BEN_LOW = 2;
    const BAND_BEN_MODERATE = 3;
    const BAND_BEN_HIGH = 4;
    const BAND_BEN_VERY_HIGH = 5;

    const RISK_BANDS_BEN = [
        self::BAND_NO_BENEFITS => self::BAND_NO_BENEFITS,
        self::BAND_BEN_VERY_LOW => self::BAND_BEN_VERY_LOW,
        self::BAND_BEN_LOW => self::BAND_BEN_LOW,
        self::BAND_BEN_MODERATE => self::BAND_BEN_MODERATE,
        self::BAND_BEN_HIGH => self::BAND_BEN_HIGH,
        self::BAND_BEN_VERY_HIGH => self::BAND_BEN_VERY_HIGH
    ];

    const RISK_BANDS_BEN_LABELS = [
        self::BAND_NO_BENEFITS => "No Benefit",
        self::BAND_BEN_VERY_LOW => "Very Low Benefit",
        self::BAND_BEN_LOW => "Low Benefit",
        self::BAND_BEN_MODERATE => "Moderate Benefit",
        self::BAND_BEN_HIGH => "High Benefit",
        self::BAND_BEN_VERY_HIGH => "Very High Benefit"
    ];


    const NOT_APPLICABLE_LABEL = "Not Applicable";

    const CACHE_PREFIX = "asub";
    const CACHE_SEP = "-";

    private $_cache;

    public function assessmentModel()
    {
        return $this->belongsTo(AssessmentModel::class);
    }

    public function assessmentSubmissionVariableSelection()
    {
        return $this->hasMany(AssessmentSubmissionVariableSelection::class);
    }

    public function assessmentSubmissionBenefitSelections()
    {
        return $this->hasMany(AssessmentSubmissionBenefitSelection::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function hasVariableValueSelection(AssessmentVariable $variable, AssessmentVariableValue $value)
    {
        return AssessmentSubmissionVariableSelection::where('assessment_submission_id', $this->id)
            ->where('assessment_variable_id', $variable->id)
            ->where('assessment_variable_value_id', $value->id)
            ->count();
    }

    public function variableValueSelectionKnowledgeWeighting(AssessmentVariable $variable, AssessmentVariableValue $value)
    {
        return AssessmentSubmissionVariableSelection::where('assessment_submission_id', $this->id)
            ->where('assessment_variable_id', $variable->id)
            ->where('assessment_variable_value_id', $value->id)
            ->first()->knowledge_weighting;
    }

    public function getBandedRanking($bandBoundary, $riskValue)
    {
        $min = NULL;
        $max = $bandBoundary;
        $riskRating = self::BAND_VERY_LOW;

        foreach (self::RISK_BANDS as $riskBand) {
            if (!$min) {
                if ($riskValue <= $max) {
                    $riskRating = $riskBand;
                    break;
                }
                $min = $max;
                $max = $min + $bandBoundary;
                continue;
            }

            if ($riskBand == count(self::RISK_BANDS)) {
                $riskRating = $riskBand;
                break;
            }

            if ($riskValue > $min && $riskValue <= $max) {
                $riskRating = $riskBand;
                break;
            }

            $min = $max;
            $max = $min + $bandBoundary;
        }
        return $riskRating;
    }

    public function getRiskRatingLabel($rating)
    {
        if (isset(self::RISK_BANDS_LABELS[$rating])) {
            return self::RISK_BANDS_LABELS[$rating];
        }

        return "";
    }

    public function getSubCategoryRiskRatingLabelStyle(AssessmentSubCategory $subCategory)
    {
        if ($this->isSubCategoryRiskRatingIgnored($subCategory)) {
            $label = self::NOT_APPLICABLE_LABEL;
        } else {
            $label = $this->getSubCategoryRiskRatingLabel($subCategory);
        }

        return strtolower(str_replace(" ", "-", $label));
    }

    public function getSubCategoryRiskRatingLabel(AssessmentSubCategory $subCategory)
    {
        if ($this->isSubCategoryRiskRatingIgnored($subCategory)) {
            return self::NOT_APPLICABLE_LABEL;
        }

        return $this->getRiskRatingLabel($this->getSubCategoryRiskRating($subCategory));
    }

    public function getSubCategoryRiskRating(AssessmentSubCategory $subCategory)
    {
        $bandBoundary = $this->getSubCategoryRiskRatingBandBoundary($subCategory);
        $riskValue = $this->getSubCategoryRiskRatingValue($subCategory);

        return $this->getBandedRanking($bandBoundary, $riskValue);
    }

    public function getSubCategoryRiskRatingValue($subCategory, $ignoreCache = FALSE)
    {
        if (!$ignoreCache && $this->hasCache(__FUNCTION__, $subCategory->assessmentCategory->id, $subCategory->id)) {
            return $this->getCache(__FUNCTION__, $subCategory->assessmentCategory->id, $subCategory->id);
        }

        $total = 0;
        foreach ($subCategory->assessmentVariables as $variable) {
            $variableSelection = $this->assessmentSubmissionVariableSelection
                ->where('assessment_variable_id', $variable->id)
                ->first();
            $selectedVariableWeight = 0;
            $knowWeight = 0;
            if ($variableSelection) {
                $selectedVariableWeight = $variableSelection->assessmentVariableValue->weighting;
                $knowWeight = $variableSelection->knowledge_weighting;
            }

            $riskValue = $variable->rank * $selectedVariableWeight * $knowWeight;
            $total += $riskValue;
        }

        $this->putCache(__FUNCTION__, $total, $subCategory->assessmentCategory->id, $subCategory->id);
        return $total;
    }

    public function isSubCategoryRiskRatingIgnored($subCategory, $ignoreCache = FALSE)
    {
        if (!$ignoreCache && $this->hasCache(__FUNCTION__, $subCategory->assessmentCategory->id, $subCategory->id)) {
            return $this->getCache(__FUNCTION__, $subCategory->assessmentCategory->id, $subCategory->id);
        }

        $ignored = TRUE;
        if ($subCategory->ignoreable) {
            foreach ($subCategory->assessmentVariables as $variable) {
                $variableSelection = $this->assessmentSubmissionVariableSelection
                    ->where('assessment_variable_id', $variable->id)
                    ->first();
                $selectedVariableWeight = 0;
                $knowWeight = 0;
                if ($variableSelection) {
                    $selectedVariableWeight = $variableSelection->assessmentVariableValue->weighting;
                    $knowWeight = $variableSelection->knowledge_weighting;
                }

                $riskValue = $variable->rank * $selectedVariableWeight * $knowWeight;
                if ($riskValue != 0) {
                    $ignored = FALSE;
                }
            }
        } else {
            $ignored = FALSE;
        }

        $this->putCache(__FUNCTION__, $ignored, $subCategory->assessmentCategory->id, $subCategory->id);
        return $ignored;
    }

    public function getSubCategoryRiskRatingBandBoundary(AssessmentSubCategory $subCat, $ignoreCache = FALSE)
    {
        if (!$ignoreCache && $this->hasCache(__FUNCTION__, $subCat->assessmentCategory->id, $subCat->id)) {
            return $this->getCache(__FUNCTION__, $subCat->assessmentCategory->id, $subCat->id);
        }

        $max = 0;
        foreach ($subCat->assessmentVariables as $variable) {
            $maxWeighting = $variable->assessmentVariableValues()->max('weighting');

            $knowWeight = 1;
            if($this->site_knowledge == self::SITE_KNOWLEDGE_NOT_WELL_KNOWN) {
                $knowWeight = self::MAX_KNOWLEDGE_WEIGHTING;
            }

            $riskValue = $variable->rank * $maxWeighting * $knowWeight;
            $max += $riskValue;
        }

        $bandBoundary = $max / count(self::RISK_BANDS);

        $this->putCache(__FUNCTION__, $bandBoundary, $subCat->assessmentCategory->id, $subCat->id);
        return $bandBoundary;
    }

    public function getCategoryRiskRatingLabelStyle(AssessmentCategory $category)
    {
        return strtolower(str_replace(" ", "-", $this->getCategoryRiskRatingLabel($category)));
    }

    public function getCategoryRiskRatingLabel(AssessmentCategory $category)
    {
        return $this->getRiskRatingLabel($this->getCategoryRiskRating($category));
    }

    public function getCategoryRiskRating(AssessmentCategory $category)
    {
        $bandBoundary = $this->getCategoryRiskRatingBandBoundary($category);
        $riskValue = $this->getCategoryRiskRatingValue($category);

        return $this->getBandedRanking($bandBoundary, $riskValue);
    }

    public function getCategoryRiskRatingValue(AssessmentCategory $category, $ignoreCache = FALSE)
    {
        if (!$ignoreCache && $this->hasCache(__FUNCTION__, $category->id)) {
            return $this->getCache(__FUNCTION__, $category->id);
        }

        $total = 0;
        foreach ($category->assessmentSubCategories as $subCategory) {
            if (!$this->isSubCategoryRiskRatingIgnored($subCategory)) {
                $subCatRiskValue = $this->getSubCategoryRiskRating($subCategory);
                if ($subCatRiskValue == self::BAND_VERY_LOW) {
                    $total += $subCatRiskValue;
                } else {
                    $total += ($subCatRiskValue * $subCategory->category_weighting);
                }
            }
        }

        $this->putCache(__FUNCTION__, $total, $category->id);
        return $total;
    }


    public function getCategoryRiskRatingBandBoundary(AssessmentCategory $category, $ignoreCache = FALSE)
    {
        if (!$ignoreCache && $this->hasCache(__FUNCTION__, $category->id)) {
            return $this->getCache(__FUNCTION__, $category->id);
        }

        $max = 0;
        foreach ($category->assessmentSubCategories as $subCategory) {
            if (!$this->isSubCategoryRiskRatingIgnored($subCategory)) {
                $max += (self::BAND_VERY_HIGH * $subCategory->category_weighting);
            }
        }

        $bandBoundary = $max / count(self::RISK_BANDS);

        $this->putCache(__FUNCTION__, $bandBoundary, $category->id);
        return $bandBoundary;
    }

    public function getAssessmentRiskRatingLabelStyle()
    {
        return strtolower(str_replace(" ", "-", $this->getAssessmentRiskRatingLabel()));
    }

    public function getAssessmentRiskRatingLabel()
    {
        return $this->getRiskRatingLabel($this->getAssessmentRiskRating());
    }

    public function getAssessmentRiskRating()
    {
        $bandBoundary = $this->getAssessmentRiskRatingBandBoundary();
        $riskValue = $this->getAssessmentRiskRatingValue();

        return $this->getBandedRanking($bandBoundary, $riskValue);
    }

    public function getAssessmentRiskRatingValue($ignoreCache = FALSE)
    {
        if (!$ignoreCache && $this->hasCache(__FUNCTION__)) {
            return $this->getCache(__FUNCTION__);
        }

        $total = 0;
        foreach ($this->assessmentModel->assessmentCategories as $category) {
            foreach ($category->assessmentSubCategories as $subCategory) {
                $subCatRiskValue = $this->getSubCategoryRiskRating($subCategory);
                if ($subCatRiskValue == self::BAND_VERY_LOW) {
                    $total += $subCatRiskValue;
                } else {
                    $total += ($subCatRiskValue * $subCategory->assessment_weighting);
                }
            }
        }

        $this->putCache(__FUNCTION__, $total);
        return $total;
    }

    public function getAssessmentRiskRatingBandBoundary($ignoreCache = FALSE)
    {
        if (!$ignoreCache && $this->hasCache(__FUNCTION__)) {
            return $this->getCache(__FUNCTION__);
        }

        $max = 0;
        foreach ($this->assessmentModel->assessmentCategories as $category) {
            foreach ($category->assessmentSubCategories as $subCategory) {
                if (!$this->isSubCategoryRiskRatingIgnored($subCategory)) {
                    $max += (self::BAND_VERY_HIGH * $subCategory->assessment_weighting);
                }
            }
        }

        $bandBoundary = $max / count(self::RISK_BANDS);

        $this->putCache(__FUNCTION__, $bandBoundary);
        return $bandBoundary;
    }

    public function putCache($key, $value, $category = 0, $subcategory = 0)
    {
        $this->_cache[$key][$category][$subcategory] = $value;
    }

    public function hasCache($key, $category = 0, $subcategory = 0)
    {
        return isset($this->_cache[$key][$category][$subcategory]);
    }

    public function getCache($key, $category = 0, $subcategory = 0)
    {
        if ($this->hasCache($key, $category, $subcategory)) {
            return $this->_cache[$key][$category][$subcategory];
        }

        return NULL;
    }

    public function flushCaches()
    {
        $this->_cache = [];
    }

    public function getAssessmentBenefitSubmissionValue(AssessmentBenefit $assessmentBenefit)
    {
        $existing = AssessmentSubmissionBenefitSelection::where('assessment_submission_id', $this->id)
            ->where('assessment_benefit_id', $assessmentBenefit->id)
            ->first();

        if ($existing) {
            return $existing->value;
        }
    }

    public function getAssessmentBenefitSubmissionValuePercent(AssessmentBenefit $assessmentBenefit)
    {
        $existing = AssessmentSubmissionBenefitSelection::where('assessment_submission_id', $this->id)
            ->where('assessment_benefit_id', $assessmentBenefit->id)
            ->first();

        if ($existing) {
            return $existing->value / $assessmentBenefit->maximum_value * 100;
        }
    }

    public function getAssessmentBenefitRatingLabelStyle()
    {
        $label = $this->getAssessmentBenefitRatingLabel();
        return strtolower(str_replace(" ", "-", $label));
    }

    public function getAssessmentBenefitRatingLabel()
    {
        if (isset(self::RISK_BANDS_BEN_LABELS[$this->getAssessmentBenefitRating()])) {
            return self::RISK_BANDS_BEN_LABELS[$this->getAssessmentBenefitRating()];
        }

        return self::RISK_BANDS_BEN_LABELS[self::BAND_NO_BENEFITS];
    }

    public function getAssessmentBenefitRating()
    {
        $greaterThanSevenCount = 0;
        $maxBenefitFound = 0;
        foreach ($this->assessmentSubmissionBenefitSelections as $assessmentBenefitSelection) {
            if ($assessmentBenefitSelection->value >= 7) {
                $greaterThanSevenCount++;
                if ($greaterThanSevenCount > 1) {
                    return self::BAND_BEN_VERY_HIGH;
                }
            }

            if ($assessmentBenefitSelection->value > $maxBenefitFound) {
                $maxBenefitFound = $assessmentBenefitSelection->value;
            }
        }

        if ($maxBenefitFound == 1) {
            return self::BAND_BEN_VERY_LOW;
        }

        if ($maxBenefitFound >= 2 && $maxBenefitFound <= 3) {
            return self::BAND_BEN_LOW;
        }

        if ($maxBenefitFound >= 4 && $maxBenefitFound <= 6) {
            return self::BAND_BEN_MODERATE;
        }

        if ($maxBenefitFound >= 7) {
            return self::BAND_BEN_HIGH;
        }

        return self::BAND_NO_BENEFITS;
    }

    public function navList()
    {
        $navlist = [];

        foreach ($this->assessmentModel->assessmentCategories()->orderBy('display_weight')->get() as $category) {
            foreach ($category->assessmentSubCategories()->orderBy('display_weight')->get() as $subCategory) {
                $navlist[] = $subCategory->id;
            }
        }

        return $navlist;
    }

    public function prevSubmit(AssessmentSubCategory $assessmentSubCategory)
    {
        $navList = $this->navList();

        $prev = 0;
        foreach ($navList as $item) {
            if ($item == $assessmentSubCategory->id) {
                return $prev;
            }

            $prev = $item;
        }

        return $prev;
    }

    public function nextSubmit(AssessmentSubCategory $assessmentSubCategory)
    {
        $navList = $this->navList();

        $prev = 0;
        foreach (array_reverse($navList) as $item) {
            if ($item == $assessmentSubCategory->id) {
                return $prev;
            }

            $prev = $item;
        }

        return $prev;
    }

    public function finalSubmit()
    {
        $navList = $this->navList();

        return array_pop($navList);
    }
}
