<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function assessmentSubmissions() {
        return $this->hasMany( AssessmentSubmission::class);
    }

    public function isAdmin() {
        $admins = explode(",", env('ADMIN_UIDS', 1));
        return in_array($this->id, $admins);
    }

    public static function boot ()
    {
        parent::boot();

        self::creating(function ($model) {

            $model->api_token = str_random(60);

        });
    }

}
