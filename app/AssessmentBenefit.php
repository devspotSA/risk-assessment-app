<?php

namespace App;

use Closure;
use Illuminate\Database\Eloquent\Model;

class AssessmentBenefit extends Model
{
    protected $fillable = [
        'name',
        'description',
        'image',
        'maximum_value',
        'display_weight',
        'assessment_model_id'
    ];

    public function assessmentModel()
    {
        return $this->belongsTo(AssessmentModel::class);
    }

    public static function importFromCSV($source_file, Closure $callback)
    {
        if (!file_exists($source_file)) {
            throw new \Exception("File not found: " . $source_file);
        }

        $row_num = 0;
        $headers = [];
        if (($handle = fopen($source_file, "r")) !== FALSE) {
            while (($row = fgetcsv($handle, 4096, ",")) !== FALSE) {
                $row_num++;
                if ($row_num == 1) {
                    $headers = $row;
                    continue;
                }

                $data = [];
                foreach ($row as $k => $v) {
                    $data[$headers[$k]] = $v;
                }

                try {
                    $assessmentModel = assessmentModel::where('name', $data['model'])->first();
                    unset($data['model']);

                    $newRow = $assessmentModel->assessmentBenefits()->create($data);
                } catch (\Exception $ex) {
                    print "Shit: Died at " . print_r($data, TRUE);
                    throw $ex;
                }

                $callback($newRow);
            }

            fclose($handle);

            // Account for the header
            return ($row_num - 1);
        }
    }
}
