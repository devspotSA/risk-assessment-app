<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Closure;
use Illuminate\Support\Facades\DB;

class AssessmentVariableValue extends Model
{
    protected $fillable = [
        'name',
        'display_weight',
        'weighting',
        'image',
        'assessment_variable_id'
    ];

    public function assessmentVariable()
    {
        return $this->belongsTo(AssessmentVariable::class);
    }

    public static function importFromCSV($source_file, Closure $callback)
    {
        if (!file_exists($source_file)) {
            throw new \Exception("File not found: " . $source_file);
        }

        $row_num = 0;
        $headers = [];
        if (($handle = fopen($source_file, "r")) !== FALSE) {
            while (($row = fgetcsv($handle, 4096, ",")) !== FALSE) {
                $row_num++;
                if ($row_num == 1) {
                    $headers = $row;
                    continue;
                }

                $data = [];
                foreach ($row as $k => $v) {
                    $data[$headers[$k]] = $v;
                }

                try {
                    $assessmentModel = assessmentModel::where('name', $data['model'])->first();
                    $assessmentCategory = AssessmentCategory::where('name', $data['category'])
                        ->where('assessment_model_id', $assessmentModel->id)->first();

                    $assessmentSubCategory = AssessmentSubCategory::where('name', $data['subcategory'])
                        ->where('assessment_category_id', $assessmentCategory->id)->first();

                    $assessmentVariable = AssessmentVariable::where('name', $data['variable'])
                        ->where('assessment_sub_category_id', $assessmentSubCategory->id)->first();

                    unset($data['model']);
                    unset($data['category']);
                    unset($data['subcategory']);
                    unset($data['variable']);


                    $newRow = $assessmentVariable->assessmentVariableValues()->create($data);
                } catch (\Exception $ex) {
                    print "Shit: Died at " . print_r($data, TRUE);
                    throw $ex;
                }
                $callback($newRow);
            }

            fclose($handle);

            // Account for the header
            return ($row_num - 1);
        }
    }
}
