<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Closure;

class AssessmentModel extends Model
{
    protected $fillable = [
        'name',
        'description',
        'image',
        'display_weight',
  ];

  public function assessmentCategories()
  {
      return $this->hasMany(AssessmentCategory::class);
  }

  public function assessmentBenefits()
  {
      return $this->hasMany(AssessmentBenefit::class);
  }

  public static function importFromCSV($source_file, Closure $callback)
  {
      if (!file_exists($source_file)) {
          throw new \Exception("File not found: " . $source_file);
      }

      $row_num = 0;
      $headers = [];
      if (($handle = fopen($source_file, "r")) !== FALSE) {
          while (($row = fgetcsv($handle, 4096, ",")) !== FALSE) {
              $row_num++;
              if ($row_num == 1) {
                  $headers = $row;
                  continue;
              }

              $data = [];
              foreach ($row as $k => $v) {
                  $data[$headers[$k]] = $v;
              }

              $newRow = AssessmentModel::create($data);
              $callback($newRow);
          }

          fclose($handle);

          // Account for the header
          return ($row_num - 1);
      }
  }
}
