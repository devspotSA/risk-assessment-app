<?php

namespace App\Http\Controllers;

use App\AssessmentCategory;
use App\AssessmentModel;
use Illuminate\Foundation\Testing\HttpException;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AssessmentCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = AssessmentCategory::simplePaginate();
        return view('assessmentCategory.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(! $request->get('parent')){
            throw new NotFoundHttpException("Model not found");
        }

        $parent = AssessmentModel::findOrFail($request->get('parent'));

        return view('assessmentCategory.create', compact('parent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required',
            'display_weight' => 'required',
            'assessment_model_id' => 'required|exists:assessment_models,id'
        ]);

        $data = $request->all();

        $model = AssessmentCategory::create( $data);

        return redirect(route('assessmentCategory.show', [ $model->id ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssessmentCategory  $assessmentCategory
     * @return \Illuminate\Http\Response
     */
    public function show(AssessmentCategory $assessmentCategory)
    {
        $model = $assessmentCategory;
        return view('assessmentCategory.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssessmentCategory  $assessmentCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(AssessmentCategory $assessmentCategory)
    {
        $model = $assessmentCategory;
        return view('assessmentCategory.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssessmentCategory  $assessmentCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssessmentCategory $assessmentCategory)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'display_weight' => 'required',
            'description' => 'required',
        ]);

        $assessmentCategory->fill($request->all());
        $assessmentCategory->save();

        return redirect(route('assessmentCategory.show', [ $assessmentCategory->id ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssessmentCategory  $assessmentCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssessmentCategory $assessmentCategory)
    {
        //
    }
}
