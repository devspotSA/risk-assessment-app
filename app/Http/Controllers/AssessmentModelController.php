<?php

namespace App\Http\Controllers;

use App\AssessmentModel;
use Illuminate\Http\Request;

class AssessmentModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = AssessmentModel::simplePaginate();
        return view('assessmentModel.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('assessmentModel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required',
            'image' => 'required|max:255',
        ]);

        $data = $request->all();

        $model = AssessmentModel::create( $data);

        return redirect(route('assessmentModel.show', [ $model->id ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssessmentModel  $assessmentModel
     * @return \Illuminate\Http\Response
     */
    public function show(AssessmentModel $assessmentModel)
    {
        $model = $assessmentModel;
        return view('assessmentModel.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssessmentModel  $assessmentModel
     * @return \Illuminate\Http\Response
     */
    public function edit(AssessmentModel $assessmentModel)
    {
        $model = $assessmentModel;
        return view('assessmentModel.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssessmentModel  $assessmentModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssessmentModel $assessmentModel)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required',
            'image' => 'required|max:255',
        ]);

        $assessmentModel->fill($request->all());
        $assessmentModel->save();

        return redirect(route('assessmentModel.show', [ $assessmentModel->id ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssessmentModel  $assessmentModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssessmentModel $assessmentModel)
    {
        //
    }

    public function hierarchy(AssessmentModel $assessmentModel) {
        $model = $assessmentModel;
        return view('assessmentModel.hierarchy', compact('model'));
    }
}
