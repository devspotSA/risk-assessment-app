<?php

namespace App\Http\Controllers;

use App\AssessmentSubmission;
use App\AssessmentSubmissionVariableSelection;
use App\AssessmentVariable;
use App\AssessmentVariableValue;
use Illuminate\Http\Request;

class ApiAssessmentVariableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssessmentVariable  $assessmentVariable
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, AssessmentVariable $assessmentVariable)
    {
        $data = $assessmentVariable->toArray();

        $data['options'] = [];
        foreach($assessmentVariable->AssessmentVariableValues()->orderBy('display_weight')->get() as $value) {
            $data['options'][] = [
                'id' => $value->id,
                'name' => $value->name,
                'weighting' => $value->weighting
            ];
        }

        if($request->get('assessment_submission_id')) {
            $existing = AssessmentSubmissionVariableSelection::where('assessment_submission_id', $request->get('assessment_submission_id'))
                ->where('assessment_variable_id', $assessmentVariable->id)
                ->first();

            $assessmentSubmission = AssessmentSubmission::find($request->get('assessment_submission_id'));
            $data['site_knowledge'] = $assessmentSubmission->site_knowledge;

            if($existing) {
                $data['current_selection'] = [
                    'id' => $existing->assessmentVariableValue->id,
                    'name' => $existing->assessmentVariableValue->name,
                    'weighting' => $existing->assessmentVariableValue->weighting,
                    'knowledge_weighting' => str_replace(".00","",$existing->knowledge_weighting)
                ];
            } else {
                if($data['site_knowledge'] == AssessmentSubmission::SITE_KNOWLEDGE_NOT_WELL_KNOWN) {
                    $data['current_selection'] = [
                        'knowledge_weighting' => "1.50"
                    ];
                } else {
                    $data['current_selection'] = [
                        'knowledge_weighting' => "1"
                    ];
                }
            }


        }

        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssessmentVariable  $assessmentVariable
     * @return \Illuminate\Http\Response
     */
    public function edit(AssessmentVariable $assessmentVariable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssessmentVariable  $assessmentVariable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssessmentVariable $assessmentVariable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssessmentVariable  $assessmentVariable
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssessmentVariable $assessmentVariable)
    {
        //
    }
}
