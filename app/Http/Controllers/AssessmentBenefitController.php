<?php

namespace App\Http\Controllers;

use App\AssessmentBenefit;
use App\AssessmentModel;
use Illuminate\Http\Request;

class AssessmentBenefitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = AssessmentBenefit::simplePaginate();
        return view('assessmentBenefit.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(! $request->get('parent')){
            throw new NotFoundHttpException("Model not found");
        }

        $parent = AssessmentModel::findOrFail($request->get('parent'));

        return view('assessmentBenefit.create', compact('parent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required',
            'maximum_value' => 'required|numeric',
            'display_weight' => 'required',
            'assessment_model_id' => 'required|exists:assessment_models,id'
        ]);

        $data = $request->all();

        $model = AssessmentBenefit::create( $data);

        return redirect(route('assessmentBenefit.show', [ $model->id ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssessmentBenefit  $assessmentBenefit
     * @return \Illuminate\Http\Response
     */
    public function show(AssessmentBenefit $assessmentBenefit)
    {
        $model = $assessmentBenefit;
        return view('assessmentBenefit.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssessmentBenefit  $assessmentBenefit
     * @return \Illuminate\Http\Response
     */
    public function edit(AssessmentBenefit $assessmentBenefit)
    {
        $model = $assessmentBenefit;
        return view('assessmentBenefit.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssessmentBenefit  $assessmentBenefit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssessmentBenefit $assessmentBenefit)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required',
            'maximum_value' => 'required|numeric',
            'display_weight' => 'required',
        ]);

        $assessmentBenefit->fill($request->all());
        $assessmentBenefit->save();

        return redirect(route('assessmentBenefit.show', [ $assessmentBenefit->id ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssessmentBenefit  $assessmentBenefit
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssessmentBenefit $assessmentBenefit)
    {
        //
    }
}
