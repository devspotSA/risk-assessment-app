<?php

namespace App\Http\Controllers;

use App\AssessmentSubCategory;
use App\AssessmentVariable;
use Illuminate\Http\Request;

class AssessmentVariableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = AssessmentVariable::simplePaginate();
        return view('assessmentVariable.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(! $request->get('parent')){
            throw new NotFoundHttpException("Model not found");
        }

        $parent = AssessmentSubCategory::findOrFail($request->get('parent'));
        return view('assessmentVariable.create', compact('parent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required',
            'rank' => 'required|numeric',
            'display_weight' => 'required|numeric',
            'assessment_sub_category_id' => 'required|exists:assessment_sub_categories,id'
        ]);

        $data = $request->all();

        $model = AssessmentVariable::create( $data);

        return redirect(route('assessmentVariable.show', [ $model->id ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssessmentVariable  $assessmentVariable
     * @return \Illuminate\Http\Response
     */
    public function show(AssessmentVariable $assessmentVariable)
    {
        $model = $assessmentVariable;
        return view('assessmentVariable.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssessmentVariable  $assessmentVariable
     * @return \Illuminate\Http\Response
     */
    public function edit(AssessmentVariable $assessmentVariable)
    {
        $model = $assessmentVariable;
        return view('assessmentVariable.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssessmentVariable  $assessmentVariable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssessmentVariable $assessmentVariable)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required',
            'rank' => 'required|numeric',
            'display_weight' => 'required|numeric',
        ]);

        $assessmentVariable->fill($request->all());
        $assessmentVariable->save();

        return redirect(route('assessmentVariable.show', [ $assessmentVariable->id ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssessmentVariable  $assessmentVariable
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssessmentVariable $assessmentVariable)
    {
        //
    }
}
