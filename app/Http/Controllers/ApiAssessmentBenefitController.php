<?php

namespace App\Http\Controllers;

use App\AssessmentBenefit;
use App\AssessmentSubmissionBenefitSelection;
use Illuminate\Http\Request;

class ApiAssessmentBenefitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  \App\AssessmentBenefit $assessmentBenefit
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, AssessmentBenefit $assessmentBenefit)
    {
        $data = $assessmentBenefit->toArray();

        if($request->get('assessment_submission_id')) {
            $existing = AssessmentSubmissionBenefitSelection::where('assessment_submission_id', $request->get('assessment_submission_id'))
                ->where('assessment_benefit_id', $assessmentBenefit->id)
                ->first();
            if($existing) {
                $data['current_selection'] = $existing->value;
            }
        }

        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssessmentBenefit  $assessmentBenefit
     * @return \Illuminate\Http\Response
     */
    public function edit(AssessmentBenefit $assessmentBenefit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssessmentBenefit  $assessmentBenefit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssessmentBenefit $assessmentBenefit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssessmentBenefit  $assessmentBenefit
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssessmentBenefit $assessmentBenefit)
    {
        //
    }
}
