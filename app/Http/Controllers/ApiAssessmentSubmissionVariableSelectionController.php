<?php

namespace App\Http\Controllers;

use App\AssessmentSubmissionVariableSelection;
use Illuminate\Http\Request;

class ApiAssessmentSubmissionVariableSelectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $existing = AssessmentSubmissionVariableSelection::where('assessment_submission_id', $request->get('assessment_submission_id'))
            ->where('assessment_variable_id', $request->get('assessment_variable_id'))
            ->first();

        if(!$existing) {
            $existing = new AssessmentSubmissionVariableSelection($request->all());
            $existing->save();
        } else {
            $existing->assessment_variable_value_id = $request->get('assessment_variable_value_id');
            $existing->knowledge_weighting = $request->get('knowledge_weighting');
            $existing->save();
        }

        return $existing;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssessmentSubmissionVariableSelection  $assessmentSubmissionVariableSelection
     * @return \Illuminate\Http\Response
     */
    public function show(AssessmentSubmissionVariableSelection $assessmentSubmissionVariableSelection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssessmentSubmissionVariableSelection  $assessmentSubmissionVariableSelection
     * @return \Illuminate\Http\Response
     */
    public function edit(AssessmentSubmissionVariableSelection $assessmentSubmissionVariableSelection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssessmentSubmissionVariableSelection  $assessmentSubmissionVariableSelection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssessmentSubmissionVariableSelection $assessmentSubmissionVariableSelection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssessmentSubmissionVariableSelection  $assessmentSubmissionVariableSelection
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssessmentSubmissionVariableSelection $assessmentSubmissionVariableSelection)
    {
        //
    }
}
