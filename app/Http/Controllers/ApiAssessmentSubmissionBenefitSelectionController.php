<?php

namespace App\Http\Controllers;

use App\AssessmentSubmissionBenefitSelection;
use Illuminate\Http\Request;

class ApiAssessmentSubmissionBenefitSelectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $existing = AssessmentSubmissionBenefitSelection::where('assessment_submission_id', $request->get('assessment_submission_id'))
            ->where('assessment_benefit_id', $request->get('assessment_benefit_id'))
            ->first();

        if(!$existing) {
            $existing = new AssessmentSubmissionBenefitSelection($request->all());
            $existing->save();
        } else {
            $existing->value = $request->get('value');
            $existing->save();
        }

        return $existing;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssessmentSubmissionBenefitSelection  $assessmentSubmissionBenefitSelection
     * @return \Illuminate\Http\Response
     */
    public function show(AssessmentSubmissionBenefitSelection $assessmentSubmissionBenefitSelection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssessmentSubmissionBenefitSelection  $assessmentSubmissionBenefitSelection
     * @return \Illuminate\Http\Response
     */
    public function edit(AssessmentSubmissionBenefitSelection $assessmentSubmissionBenefitSelection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssessmentSubmissionBenefitSelection  $assessmentSubmissionBenefitSelection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssessmentSubmissionBenefitSelection $assessmentSubmissionBenefitSelection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssessmentSubmissionBenefitSelection  $assessmentSubmissionBenefitSelection
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssessmentSubmissionBenefitSelection $assessmentSubmissionBenefitSelection)
    {
        //
    }
}
