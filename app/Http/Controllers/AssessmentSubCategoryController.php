<?php

namespace App\Http\Controllers;

use App\AssessmentCategory;
use App\AssessmentSubCategory;
use Illuminate\Http\Request;

class AssessmentSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = AssessmentSubCategory::simplePaginate();
        return view('assessmentSubCategory.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(! $request->get('parent')){
            throw new NotFoundHttpException("Model not found");
        }

        $parent = AssessmentCategory::findOrFail($request->get('parent'));

        return view('assessmentSubCategory.create',compact('parent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required',
            'display_weight' => 'required',
            'category_weighting' => 'required|numeric',
            'assessment_weighting' => 'required|numeric',
            'assessment_category_id' => 'required|exists:assessment_categories,id'
        ]);

        $data = $request->all();
        $data['ignoreable'] = $request->get('ignoreable') ? 1 : 0;

        $model = AssessmentSubCategory::create( $data);

        return redirect(route('assessmentSubCategory.show', [ $model->id ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssessmentSubCategory  $assessmentSubCategory
     * @return \Illuminate\Http\Response
     */
    public function show(AssessmentSubCategory $assessmentSubCategory)
    {
        $model = $assessmentSubCategory;
        return view('assessmentSubCategory.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssessmentSubCategory  $assessmentSubCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(AssessmentSubCategory $assessmentSubCategory)
    {
        $model = $assessmentSubCategory;
        return view('assessmentSubCategory.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssessmentSubCategory  $assessmentSubCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssessmentSubCategory $assessmentSubCategory)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required',
            'display_weight' => 'required',
            'category_weighting' => 'required|numeric',
            'assessment_weighting' => 'required|numeric'
        ]);

        $data = $request->all();
        $data['ignoreable'] = $request->get('ignoreable') ? 1 : 0;

        $assessmentSubCategory->fill($data);
        $assessmentSubCategory->save();

        return redirect(route('assessmentSubCategory.show', [ $assessmentSubCategory->id ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssessmentSubCategory  $assessmentSubCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssessmentSubCategory $assessmentSubCategory)
    {
        //
    }
}
