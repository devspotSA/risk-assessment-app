<?php

namespace App\Http\Controllers;

use App\AssessmentVariable;
use App\AssessmentVariableValue;
use Illuminate\Http\Request;

class AssessmentVariableValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = AssessmentVariableValue::simplePaginate();
        return view('assessmentVariableValue.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(! $request->get('parent')){
            throw new NotFoundHttpException("Model not found");
        }

        $parent = AssessmentVariable::findOrFail($request->get('parent'));
        return view('assessmentVariableValue.create', compact('parent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'weighting' => 'required|numeric',
            'display_weight' => 'required|numeric',
            'assessment_variable_id' => 'required|exists:assessment_variables,id'
        ]);

        $data = $request->all();
        $model = AssessmentVariableValue::create( $data);

        return redirect(route('assessmentVariableValue.show', [ $model->id ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssessmentVariableValue  $assessmentVariableValue
     * @return \Illuminate\Http\Response
     */
    public function show(AssessmentVariableValue $assessmentVariableValue)
    {
        $model = $assessmentVariableValue;
        return view('assessmentVariableValue.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssessmentVariableValue  $assessmentVariableValue
     * @return \Illuminate\Http\Response
     */
    public function edit(AssessmentVariableValue $assessmentVariableValue)
    {
        $model = $assessmentVariableValue;
        return view('assessmentVariableValue.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssessmentVariableValue  $assessmentVariableValue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssessmentVariableValue $assessmentVariableValue)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'weighting' => 'required|numeric',
            'display_weight' => 'required|numeric',
        ]);

        $assessmentVariableValue->fill($request->all());
        $assessmentVariableValue->save();

        return redirect(route('assessmentVariableValue.show', [ $assessmentVariableValue->id ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssessmentVariableValue  $assessmentVariableValue
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssessmentVariableValue $assessmentVariableValue)
    {
        //
    }
}
