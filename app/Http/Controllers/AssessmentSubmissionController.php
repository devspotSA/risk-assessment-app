<?php

namespace App\Http\Controllers;

use App\AssessmentSubCategory;
use App\AssessmentSubmission;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AssessmentSubmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param User|null $user
     * @return \Illuminate\Http\Response
     */
    public function create(User $user = NULL)
    {
        if(!$user->id) { $user = Auth::user(); }
        return view('assessmentSubmission.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required',
            'assessment_model_id' => 'required',
            'user_id' => 'required'
        ]);

        $user = User::find($request->get('user_id'));
        $assessmentSubmission = $user->assessmentSubmissions()->create($request->all());

        return redirect(route('assessmentSubmission.show', [ $assessmentSubmission->id ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssessmentSubmission  $assessmentSubmission
     * @return \Illuminate\Http\Response
     */
    public function show(AssessmentSubmission $assessmentSubmission)
    {
        $assessmentSubmission->flushCaches();
        return view('assessmentSubmission.show', compact('assessmentSubmission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssessmentSubmission  $assessmentSubmission
     * @return \Illuminate\Http\Response
     */
    public function edit(AssessmentSubmission $assessmentSubmission)
    {
        return view('assessmentSubmission.edit', compact('assessmentSubmission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssessmentSubmission  $assessmentSubmission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssessmentSubmission $assessmentSubmission)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required',
        ]);

        $assessmentSubmission->fill( $request->all());
        $assessmentSubmission->save();

        return redirect(route('assessmentSubmission.show', [ $assessmentSubmission->id ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssessmentSubmission  $assessmentSubmission
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssessmentSubmission $assessmentSubmission)
    {
        //
    }

    /**
     * @param AssessmentSubmission $assessmentSubmission
     * @param AssessmentSubCategory $assessmentSubCategory
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function submit(AssessmentSubmission $assessmentSubmission, AssessmentSubCategory $assessmentSubCategory)
    {
      return view('assessmentSubmission.submit', compact('assessmentSubmission', 'assessmentSubCategory'));
    }

    /**
     * @param AssessmentSubmission $assessmentSubmission
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function submitBenefits(AssessmentSubmission $assessmentSubmission)
    {
        return view('assessmentSubmission.submitBenefits', compact('assessmentSubmission'));
    }

    public function dump(assessmentSubmission $assessmentSubmission) {
        return view('assessmentSubmission.dump', compact('assessmentSubmission'));
    }
}
