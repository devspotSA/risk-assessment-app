<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param User|null $user
     * @return \Illuminate\Http\Response
     */
    public function index(User $user = NULL)
    {
        if(!$user->id) { $user = Auth::user(); }

        return view('home', compact('user') );
    }
}
