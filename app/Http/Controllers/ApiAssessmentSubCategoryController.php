<?php

namespace App\Http\Controllers;

use App\AssessmentSubCategory;
use App\AssessmentSubmission;
use Illuminate\Http\Request;

class ApiAssessmentSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  \App\AssessmentSubCategory $assessmentSubCategory
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, AssessmentSubCategory $assessmentSubCategory)
    {
        $data = $assessmentSubCategory->toArray();

        if ($request->get('assessment_submission_id')) {
            $assessmentSubmission = AssessmentSubmission::find($request->get('assessment_submission_id'));
            if ($assessmentSubmission) {
                $data['riskLevel'] = [
                    'level' => $assessmentSubmission->getSubCategoryRiskRatingLabel($assessmentSubCategory),
                    'style' => $assessmentSubmission->getSubCategoryRiskRatingLabelStyle($assessmentSubCategory)
                ];
            }
        }

        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssessmentSubCategory $assessmentSubCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(AssessmentSubCategory $assessmentSubCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\AssessmentSubCategory $assessmentSubCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssessmentSubCategory $assessmentSubCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssessmentSubCategory $assessmentSubCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssessmentSubCategory $assessmentSubCategory)
    {
        //
    }
}
