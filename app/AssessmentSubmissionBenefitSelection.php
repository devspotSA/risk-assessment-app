<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssessmentSubmissionBenefitSelection extends Model
{
    protected $fillable = [
        'assessment_submission_id',
        'assessment_benefit_id',
        'value',
    ];

    public function assessmentSubmission() {
        return $this->belongsTo(AssessmentSubmission::class);
    }

    public function assessmentBenefit() {
        return $this->belongsTo(AssessmentBenefit::class);
    }
}
