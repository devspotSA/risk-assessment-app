<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('root');

Auth::routes();

Route::get('/home/{user?}', 'HomeController@index')->name('home');

Route::resource('user','UserController');
Route::resource('assessmentModel','AssessmentModelController');
Route::resource('assessmentCategory','AssessmentCategoryController');
Route::resource('assessmentSubCategory','AssessmentSubCategoryController');
Route::resource('assessmentVariable','AssessmentVariableController');
Route::resource('assessmentVariableValue','AssessmentVariableValueController');
Route::resource('assessmentBenefit','AssessmentBenefitController');

Route::resource('assessmentVariable', 'AssessmentVariableController');
Route::resource('assessmentSubmission', 'AssessmentSubmissionController');

Route::get('/assessmentDump/{assessmentSubmission}', 'AssessmentSubmissionController@dump');

Route::get('methodology/{assessmentModel}','AssessmentModelController@hierarchy')->name('methodology');

Route::get('/submit/{assessmentSubmission}/{assessmentSubCategory}/','AssessmentSubmissionController@submit')->name('submit');
Route::get('/submitBenefits/{assessmentSubmission}','AssessmentSubmissionController@submitBenefits')->name('submitBenefits');
